﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace FixedDD.Models
{
    [Table("FlightSearchResults")]
    public class FlightSearchResults
    {
        public int id { get; set; }

        public string Triptype { get; set; }
        public string OrgDestFrom { get; set; }
        public string OrgDestTo { get; set; }
        public string DepartureLocation { get; set; }
        public string DepartureCityName { get; set; }
        public string DepAirportCode { get; set; }
        public string DepartureAirportName { get; set; }
        public string DepartureTerminal { get; set; }
        public string ArrivalLocation { get; set; }
        public string ArrivalCityName { get; set; }
        public string ArrAirportCode { get; set; }
        public string ArrivalAirportName { get; set; }
        public string ArrivalTerminal { get; set; }
        public string Departure_Date { get; set; }
        public string DepartureTime { get; set; }
        public string Arrival_Date { get; set; }
        public string ArrivalTime { get; set; }
        public string MarketingCarrier { get; set; }
        public string OperatingCarrier { get; set; }
        public string FlightIdentification { get; set; }
        public string ValiDatingCarrier { get; set; }
        public string AirLineName { get; set; }
        public string ElectronicTicketing { get; set; }
        public string ProductDetailQualifier { get; set; }
        public string getVia { get; set; }
        public string BagInfo { get; set; }
        public string ProviderUserID { get; set; }
        public string AdtCabin { get; set; }
        public string ChdCabin { get; set; }
        public string InfCabin { get; set; }
        public string AdtRbd { get; set; }
        public string ChdRbd { get; set; }
        public string InfRbd { get; set; }
        public string AdtFareType { get; set; }
        public string AdtFarebasis { get; set; }
        public string ChdfareType { get; set; }
        public string ChdFarebasis { get; set; }
        public string InfFareType { get; set; }
        public string InfFarebasis { get; set; }
        public string InfFar { get; set; }
        public string ChdFar { get; set; }
        public string AdtFar { get; set; }
        public string AdtBreakPoint { get; set; }
        public string AdtAvlStatus { get; set; }
        public string ChdBreakPoint { get; set; }
        public string ChdAvlStatus { get; set; }
        public string InfBreakPoint { get; set; }
        public string InfAvlStatus { get; set; }
        public string fareBasis { get; set; }
        public string FBPaxType { get; set; }
        public string RBD { get; set; }
        public string FareRule { get; set; }
        public string FareDet { get; set; }
        public string CreatedByName { get; set; }
        public string SupMarkup { get; set; }
        public string MarkupWithGst { get; set; }
        public string FlightNo { get; set; }
        public string ClassType { get; set; }
        public double? Basicfare { get; set; }
        public double? YQ { get; set; }
        public double? YR { get; set; }
        public double? WO { get; set; }
        public double? OT { get; set; }
        public double? GROSS_Total { get; set; }
        public double? Admin_Markup { get; set; }
        public string Markup_Type { get; set; }
        public double? Markup { get; set; }
        public double? Grand_Total { get; set; }
        public DateTime? DepartureDate { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public string Total_Seats { get; set; }
        public string Used_Seat { get; set; }
        public string Avl_Seat { get; set; }
        public DateTime? valid_Till { get; set; }
        public bool Status { get; set; }
        public string Rt_AirLineName { get; set; }
        public string Rt_FlightNo { get; set; }
        public string Rt_Departure_Date { get; set; }
        public string Rt_DepartureTime { get; set; }
        public string Rt_Arrival_Date { get; set; }
        public string Rt_ArrivalTime { get; set; }
        public string Rt_DepartureTerminal { get; set; }
        public string Rt_ArrivalTerminal { get; set; }
        public string Rt_RBD { get; set; }
        public string Rt_fareBasis { get; set; }
        public string Rt_ClassType { get; set; }
        public bool FixedDepStatus { get; set; }
        public bool? Connect { get; set; }
        public bool? TempStatus { get; set; }
        public int SequenceNumber { get; set; }
        public string RefNo { get; set; }
        public string DepartureCityNameRef { get; set; }
        public string DepAirportCodeRef { get; set; }
        public string ArrivalCityNameRef { get; set; }
        public string ArrAirportCodeRef { get; set; }
        public string Duration { get; set; }
        public int? Leg { get; set; }
        public string CreatedByUserid { get; set; }
        public double InfFare { get; set; }
        public double infBfare { get; set; }
        public double InfTax { get; set; }
        public double InfMgtFee { get; set; }
        public bool IsPrimary { get; set; }
    }
}