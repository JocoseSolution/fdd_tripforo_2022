﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace FixedDD.Models
{
    [Table("AdtFareDetails")]
    public class AdtFareDetails
    {
        public int id { get; set; }
        public double? AdtFare { get; set; }

        public double? AdtBfare { get; set; }

        public double? AdtTax { get; set; }

        public double? AdtFSur { get; set; }//add TO Adt_Tax LIKE :-YQ:1#YR:1#WO:1#IN:1#Q:1#JN:1#OT:1#

        public double? AdtYR { get; set; }

        public double? AdtWO { get; set; }

        public double? AdtIN { get; set; }

        [DefaultValue(0)]

        public double? AdtQ { get; set; }

        public double? AdtJN { get; set; }

        public double? AdtOT { get; set; }//// added TO TABLE

        [DefaultValue(0)]

        public double? AdtSrvTax { get; set; }   // added TO TABLE

        [DefaultValue(0)]

        public double? AdtSrvTax1 { get; set; }   // added TO TABLE

        public double? AdtEduCess { get; set; }

        public double? AdtHighEduCess { get; set; }

        public double? AdtTF { get; set; }

        public double? ADTAdminMrk { get; set; }

        public double? ADTAgentMrk { get; set; }

        public double? ADTDistMrk { get; set; }

        [DefaultValue(0)]

        public double? AdtDiscount { get; set; }  //-AdtComm

        [DefaultValue(0)]

        public double? AdtDiscount1 { get; set; }

        public double? AdtCB { get; set; }
    }
}