﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixedDD.Models
{
    public class Sector_wise
    {
        
        public List<string> Sector { get; set; }

        public List<Sector_wise> SepSector { get; set; }
    }
}