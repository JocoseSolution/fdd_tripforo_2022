﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using FixedDD.Models;

namespace FixedDD.Helper
{
    public static class AccountHelper
    {
        public static string connection_Extranet = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;

        private static SqlConnection connection = new SqlConnection(connection_Extranet);
        private static SqlCommand command { get; set; }
        private static SqlDataAdapter objAdapter { get; set; }
        private static DataSet objDS { get; set; }
        private static DataTable tempObjDT { get; set; }

        public static DataTable GetAgentRegisterDetails(string userid, string password)
        {
            try
            {
                command = new SqlCommand("UserLoginNew", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@uid", userid);
                command.Parameters.AddWithValue("@pwd", password);
                //command.Parameters.AddWithValue("@IPAddress", "");
                //command.Parameters.AddWithValue("@ActionType", "GETUSERTYPE");
                //command.Parameters.Add("@Msg", SqlDbType.VarChar,1000).Direction = ParameterDirection.Output;

                objAdapter = new SqlDataAdapter();
                objAdapter.SelectCommand = command;
                tempObjDT = new DataTable();

                connection.Open();
                objAdapter.Fill(tempObjDT);
                // msg = command.Parameters["@Msg"].Value.ToString();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            connection.Close();

            return tempObjDT;
        }

        public static DataSet GetTicektReport(FlightTicketFilter filter)
        {
            try
            {
                command = new SqlCommand("USP_GetTicketDetail_Supp", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                command.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                command.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                command.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                command.Parameters.Add(new SqlParameter("@OderId", (!string.IsNullOrEmpty(filter.OrderId) ? filter.OrderId : string.Empty)));
                command.Parameters.Add(new SqlParameter("@PNR", (!string.IsNullOrEmpty(filter.PNR) ? filter.PNR : string.Empty)));
                command.Parameters.Add(new SqlParameter("@AirlinePNR", (!string.IsNullOrEmpty(filter.AirlinePNR) ? filter.AirlinePNR : string.Empty)));
                command.Parameters.Add(new SqlParameter("@PaxName", (!string.IsNullOrEmpty(filter.PaxName) ? filter.PaxName : string.Empty)));
                command.Parameters.Add(new SqlParameter("@TicketNo", (!string.IsNullOrEmpty(filter.TicketNo) ? filter.TicketNo : string.Empty)));
                command.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : string.Empty)));
                command.Parameters.Add(new SqlParameter("@Trip", (!string.IsNullOrEmpty(filter.Trip) ? filter.Trip : string.Empty)));
                command.Parameters.Add(new SqlParameter("@Status", (!string.IsNullOrEmpty(filter.Status) ? filter.Status : string.Empty)));
                command.Parameters.Add(new SqlParameter("@Supplierid", (!string.IsNullOrEmpty(filter.Suppliorid) ? filter.Suppliorid : string.Empty)));
                objAdapter = new SqlDataAdapter();
                objAdapter.SelectCommand = command;
                objDS = new DataSet();
                objAdapter.Fill(objDS, "GetTicketDetail_Intl");
                tempObjDT = objDS.Tables["GetTicketDetail_Intl"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return objDS;
        }

        public static DataSet GetTicektReportSuppAdmin(FlightTicketFilter filter)
        {
            try
            {
                command = new SqlCommand("USP_GetTicketDetail_SuppAdmin", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                command.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                command.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                command.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                command.Parameters.Add(new SqlParameter("@OderId", (!string.IsNullOrEmpty(filter.OrderId) ? filter.OrderId : string.Empty)));
                command.Parameters.Add(new SqlParameter("@PNR", (!string.IsNullOrEmpty(filter.PNR) ? filter.PNR : string.Empty)));
                command.Parameters.Add(new SqlParameter("@AirlinePNR", (!string.IsNullOrEmpty(filter.AirlinePNR) ? filter.AirlinePNR : string.Empty)));
                command.Parameters.Add(new SqlParameter("@PaxName", (!string.IsNullOrEmpty(filter.PaxName) ? filter.PaxName : string.Empty)));
                command.Parameters.Add(new SqlParameter("@TicketNo", (!string.IsNullOrEmpty(filter.TicketNo) ? filter.TicketNo : string.Empty)));
                command.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentId) ? filter.AgentId : string.Empty)));
                command.Parameters.Add(new SqlParameter("@Trip", (!string.IsNullOrEmpty(filter.Trip) ? filter.Trip : string.Empty)));
                command.Parameters.Add(new SqlParameter("@Status", (!string.IsNullOrEmpty(filter.Status) ? filter.Status : string.Empty)));
                command.Parameters.Add(new SqlParameter("@Supplierid", (!string.IsNullOrEmpty(filter.Suppliorid) ? filter.Suppliorid : string.Empty)));
                objAdapter = new SqlDataAdapter();
                objAdapter.SelectCommand = command;
                objDS = new DataSet();
                objAdapter.Fill(objDS, "GetTicketDetail_Intl");
                tempObjDT = objDS.Tables["GetTicketDetail_Intl"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return objDS;
        }

        public static List<FlightTicketReportModel> GetFlightSuppAminTicketReports(FlightTicketFilter filter, ref int totalCount)
        {
            List<FlightTicketReportModel> result = new List<FlightTicketReportModel>();
            try
            {
                DataTable dtFlightTicket = GetTicektReportSuppAdmin(filter).Tables[0];
                totalCount = dtFlightTicket.Rows.Count;
                if (dtFlightTicket.Rows.Count > 0)
                {
                    for (int i = 0; i < dtFlightTicket.Rows.Count; i++)
                    {
                        FlightTicketReportModel model = new FlightTicketReportModel();
                        model.OrderId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["OrderId"].ToString()) ? dtFlightTicket.Rows[i]["OrderId"].ToString() : string.Empty;
                        model.TotalAfterDis = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["TotalAfterDis"].ToString()) ? Convert.ToDecimal(dtFlightTicket.Rows[i]["TotalAfterDis"].ToString()) : 0;
                        model.TotalBookingCost = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["TotalBookingCost"].ToString()) ? Convert.ToDecimal(dtFlightTicket.Rows[i]["TotalBookingCost"].ToString()) : 0;
                        model.sector = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["sector"].ToString()) ? dtFlightTicket.Rows[i]["sector"].ToString() : string.Empty;
                        model.AgentId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgentId"].ToString()) ? dtFlightTicket.Rows[i]["AgentId"].ToString() : string.Empty;
                        model.AgencyName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgencyName"].ToString()) ? dtFlightTicket.Rows[i]["AgencyName"].ToString() : string.Empty;
                        model.AgentType = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgentType"].ToString()) ? dtFlightTicket.Rows[i]["AgentType"].ToString() : string.Empty;
                        model.PaxId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PaxId"].ToString()) ? Convert.ToInt32(dtFlightTicket.Rows[i]["PaxId"].ToString()) : 0;
                        model.Status = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Status"].ToString()) ? dtFlightTicket.Rows[i]["Status"].ToString() : string.Empty;
                        model.CreateDate = dtFlightTicket.Rows[i]["CreateDate"].ToString();
                        model.VC = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["VC"].ToString()) ? dtFlightTicket.Rows[i]["VC"].ToString() : string.Empty;
                        model.GdsPnr = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["GdsPnr"].ToString()) ? dtFlightTicket.Rows[i]["GdsPnr"].ToString() : string.Empty;
                        model.TicketNo = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["TicketNumber"].ToString()) ? dtFlightTicket.Rows[i]["TicketNumber"].ToString() : string.Empty;
                        model.PaxType = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PaxType"].ToString()) ? dtFlightTicket.Rows[i]["PaxType"].ToString() : string.Empty;
                        model.PgFName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["FName"].ToString()) ? dtFlightTicket.Rows[i]["FName"].ToString() : string.Empty;
                        model.PgLName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["LName"].ToString()) ? dtFlightTicket.Rows[i]["LName"].ToString() : string.Empty;
                        model.ExecutiveId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["ExecutiveId"].ToString()) ? dtFlightTicket.Rows[i]["ExecutiveId"].ToString() : string.Empty;
                        model.Trip = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Trip"].ToString()) ? dtFlightTicket.Rows[i]["Trip"].ToString() : string.Empty;
                        model.RejectedRemark = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["RejectedRemark"].ToString()) ? dtFlightTicket.Rows[i]["RejectedRemark"].ToString() : string.Empty;
                        model.Provider = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Provider"].ToString()) ? dtFlightTicket.Rows[i]["Provider"].ToString() : string.Empty;
                        model.FareType = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["FTYPE"].ToString()) ? dtFlightTicket.Rows[i]["FTYPE"].ToString() : string.Empty;
                        model.PName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PName"].ToString()) ? dtFlightTicket.Rows[i]["PName"].ToString() : string.Empty;
                        model.JourneyDate = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["JourneyDate"].ToString()) ? dtFlightTicket.Rows[i]["JourneyDate"].ToString() : string.Empty;
                        model.PaymentMode = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PaymentMode"].ToString()) ? dtFlightTicket.Rows[i]["PaymentMode"].ToString() : string.Empty;
                        model.Suppliorid = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["SearchId"].ToString()) ? dtFlightTicket.Rows[i]["SearchId"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
        public static DataTable GetSupliorbalance(string userid)
        {
            try
            {
                command = new SqlCommand("GetSupplierBalance", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@userid", (!string.IsNullOrEmpty(userid) ? userid : string.Empty)));
                objAdapter = new SqlDataAdapter();
                objAdapter.SelectCommand = command;
                objDS = new DataSet();
                objAdapter.Fill(objDS, "SupplierBalance");
                tempObjDT = objDS.Tables["SupplierBalance"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return tempObjDT;
        }

        public static DataSet GetLedgerReport(FlightLedgerFilter filter)
        {
            try
            {
                command = new SqlCommand("GetLedgerDetail", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                command.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                command.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                command.Parameters.Add(new SqlParameter("@ToDate    ", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                command.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentID) ? filter.AgentID : string.Empty)));
                command.Parameters.Add(new SqlParameter("@BookingType", (!string.IsNullOrEmpty(filter.BookingType) ? filter.BookingType : string.Empty)));
                command.Parameters.Add(new SqlParameter("@SearchType", (!string.IsNullOrEmpty(filter.SearchType) ? filter.SearchType : string.Empty)));
                objAdapter = new SqlDataAdapter();
                objAdapter.SelectCommand = command;
                objDS = new DataSet();
                objAdapter.Fill(objDS, "LedgerDetail");
                tempObjDT = objDS.Tables["LedgerDetail"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return objDS;
        }

        public static DataSet GetLedgerReportAllSuppior(FlightLedgerFilter filter)
        {
            try
            {
                command = new SqlCommand("GetLedgerDetailForAllSupplior", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@usertype", (!string.IsNullOrEmpty(filter.UserType) ? filter.UserType : string.Empty)));
                command.Parameters.Add(new SqlParameter("@LoginID", (!string.IsNullOrEmpty(filter.Loginid) ? filter.Loginid : string.Empty)));
                command.Parameters.Add(new SqlParameter("@FormDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                command.Parameters.Add(new SqlParameter("@ToDate    ", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                command.Parameters.Add(new SqlParameter("@AgentId", (!string.IsNullOrEmpty(filter.AgentID) ? filter.AgentID : string.Empty)));
                command.Parameters.Add(new SqlParameter("@BookingType", (!string.IsNullOrEmpty(filter.BookingType) ? filter.BookingType : string.Empty)));
                command.Parameters.Add(new SqlParameter("@SearchType", (!string.IsNullOrEmpty(filter.SearchType) ? filter.SearchType : string.Empty)));
                objAdapter = new SqlDataAdapter();
                objAdapter.SelectCommand = command;
                objDS = new DataSet();
                objAdapter.Fill(objDS, "LedgerDetailForAll");
                tempObjDT = objDS.Tables["LedgerDetailForAll"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return objDS;
        }

        public static DataTable GetHoldTicektReport(FlightTicketFilter filter)
        {
            try
            {
                command = new SqlCommand("Sp_Updateholdfixedbooking", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@Suppid", (!string.IsNullOrEmpty(filter.Suppliorid) ? filter.Suppliorid : string.Empty)));
                objAdapter = new SqlDataAdapter();
                objAdapter.SelectCommand = command;
                objDS = new DataSet();
                objAdapter.Fill(objDS, "Updateholdfixedbooking");
                tempObjDT = objDS.Tables["Updateholdfixedbooking"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return tempObjDT;
        }
        public static List<FlightLedgerReportModel> GetFlightLedgerReports(FlightLedgerFilter filter, ref int totalCount)
        {
            List<FlightLedgerReportModel> result = new List<FlightLedgerReportModel>();
            try
            {
                DataTable dtFlightTicket = GetLedgerReport(filter).Tables[0];
                totalCount = dtFlightTicket.Rows.Count;
                if (dtFlightTicket.Rows.Count > 0)
                {
                    for (int i = 0; i < dtFlightTicket.Rows.Count; i++)
                    {
                        FlightLedgerReportModel model = new FlightLedgerReportModel();
                        model.AgencyID = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgencyID"].ToString()) ? dtFlightTicket.Rows[i]["AgencyID"].ToString() : string.Empty;
                        model.AgencyName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgencyName"].ToString()) ? dtFlightTicket.Rows[i]["AgencyName"].ToString() : string.Empty;
                        model.InvoiceNo = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["InvoiceNo"].ToString()) ? dtFlightTicket.Rows[i]["InvoiceNo"].ToString() : string.Empty;
                        model.PnrNo = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PnrNo"].ToString()) ? dtFlightTicket.Rows[i]["PnrNo"].ToString() : string.Empty;
                        model.Aircode = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Aircode"].ToString()) ? dtFlightTicket.Rows[i]["Aircode"].ToString() : string.Empty;
                        model.TicketNo = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["TicketNo"].ToString()) ? dtFlightTicket.Rows[i]["TicketNo"].ToString() : string.Empty;
                        model.Debit = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Debit"].ToString()) ? dtFlightTicket.Rows[i]["Debit"].ToString() : string.Empty;
                        model.Credit = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Credit"].ToString()) ? dtFlightTicket.Rows[i]["Credit"].ToString() : string.Empty;
                        model.Aval_Balance = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Aval_Balance"].ToString()) ? dtFlightTicket.Rows[i]["Aval_Balance"].ToString() : string.Empty;
                        model.BookingType = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["BookingType"].ToString()) ? dtFlightTicket.Rows[i]["BookingType"].ToString() : string.Empty;
                        model.CreatedDate = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["CreatedDate"].ToString()) ? dtFlightTicket.Rows[i]["CreatedDate"].ToString() : string.Empty;
                        model.Remark = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Remark"].ToString()) ? dtFlightTicket.Rows[i]["Remark"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }

        public static List<FlightLedgerReportModel> GetFlightSuppliorLedgerReports(FlightLedgerFilter filter, ref int totalCount)
        {
            List<FlightLedgerReportModel> result = new List<FlightLedgerReportModel>();
            try
            {
                DataTable dtRecord = GetLedgerReportAllSuppior(filter).Tables[0];
                totalCount = dtRecord.Rows.Count;
                DataRow[] drFilter = dtRecord.Select("IsSupplier=True");
                if (!string.IsNullOrEmpty(filter.AgentID))
                {

                }
              
                DataTable dtFlightTicket = drFilter.CopyToDataTable();
                if (dtFlightTicket.Rows.Count > 0)
                {
                    for (int i = 0; i < dtFlightTicket.Rows.Count; i++)
                    {
                        FlightLedgerReportModel model = new FlightLedgerReportModel();
                        model.AgencyID = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgencyID"].ToString()) ? dtFlightTicket.Rows[i]["AgencyID"].ToString() : string.Empty;
                        model.AgencyName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgencyName"].ToString()) ? dtFlightTicket.Rows[i]["AgencyName"].ToString() : string.Empty;
                        model.InvoiceNo = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["InvoiceNo"].ToString()) ? dtFlightTicket.Rows[i]["InvoiceNo"].ToString() : string.Empty;
                        model.PnrNo = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PnrNo"].ToString()) ? dtFlightTicket.Rows[i]["PnrNo"].ToString() : string.Empty;
                        model.Aircode = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Aircode"].ToString()) ? dtFlightTicket.Rows[i]["Aircode"].ToString() : string.Empty;
                        model.TicketNo = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["TicketNo"].ToString()) ? dtFlightTicket.Rows[i]["TicketNo"].ToString() : string.Empty;
                        model.Debit = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Debit"].ToString()) ? dtFlightTicket.Rows[i]["Debit"].ToString() : string.Empty;
                        model.Credit = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Credit"].ToString()) ? dtFlightTicket.Rows[i]["Credit"].ToString() : string.Empty;
                        model.Aval_Balance = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Aval_Balance"].ToString()) ? dtFlightTicket.Rows[i]["Aval_Balance"].ToString() : string.Empty;
                        model.BookingType = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["BookingType"].ToString()) ? dtFlightTicket.Rows[i]["BookingType"].ToString() : string.Empty;
                        model.CreatedDate = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["CreatedDate"].ToString()) ? dtFlightTicket.Rows[i]["CreatedDate"].ToString() : string.Empty;
                        model.Remark = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Remark"].ToString()) ? dtFlightTicket.Rows[i]["Remark"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
        public static List<FlightTicketReportModel> GetFlightTicketReports(FlightTicketFilter filter, ref int totalCount)
        {
            List<FlightTicketReportModel> result = new List<FlightTicketReportModel>();
            try
            {
                DataTable dtFlightTicket = GetTicektReport(filter).Tables[0];
                totalCount = dtFlightTicket.Rows.Count;
                if (dtFlightTicket.Rows.Count > 0)
                {
                    for (int i = 0; i < dtFlightTicket.Rows.Count; i++)
                    {
                        FlightTicketReportModel model = new FlightTicketReportModel();
                        model.OrderId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["OrderId"].ToString()) ? dtFlightTicket.Rows[i]["OrderId"].ToString() : string.Empty;
                        model.TotalAfterDis = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["TotalAfterDis"].ToString()) ? Convert.ToDecimal(dtFlightTicket.Rows[i]["TotalAfterDis"].ToString()) : 0;
                        model.TotalBookingCost = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["TotalBookingCost"].ToString()) ? Convert.ToDecimal(dtFlightTicket.Rows[i]["TotalBookingCost"].ToString()) : 0;
                        model.sector = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["sector"].ToString()) ? dtFlightTicket.Rows[i]["sector"].ToString() : string.Empty;
                        model.AgentId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgentId"].ToString()) ? dtFlightTicket.Rows[i]["AgentId"].ToString() : string.Empty;
                        model.AgencyName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgencyName"].ToString()) ? dtFlightTicket.Rows[i]["AgencyName"].ToString() : string.Empty;
                        model.AgentType = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgentType"].ToString()) ? dtFlightTicket.Rows[i]["AgentType"].ToString() : string.Empty;
                        model.PaxId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PaxId"].ToString()) ? Convert.ToInt32(dtFlightTicket.Rows[i]["PaxId"].ToString()) : 0;
                        model.Status = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Status"].ToString()) ? dtFlightTicket.Rows[i]["Status"].ToString() : string.Empty;
                        model.CreateDate = dtFlightTicket.Rows[i]["CreateDate"].ToString();
                        model.VC = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["VC"].ToString()) ? dtFlightTicket.Rows[i]["VC"].ToString() : string.Empty;
                        model.GdsPnr = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["GdsPnr"].ToString()) ? dtFlightTicket.Rows[i]["GdsPnr"].ToString() : string.Empty;
                        model.TicketNo = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["TicketNumber"].ToString()) ? dtFlightTicket.Rows[i]["TicketNumber"].ToString() : string.Empty;
                        model.PaxType = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PaxType"].ToString()) ? dtFlightTicket.Rows[i]["PaxType"].ToString() : string.Empty;
                        model.PgFName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["FName"].ToString()) ? dtFlightTicket.Rows[i]["FName"].ToString() : string.Empty;
                        model.PgLName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["LName"].ToString()) ? dtFlightTicket.Rows[i]["LName"].ToString() : string.Empty;
                        model.ExecutiveId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["ExecutiveId"].ToString()) ? dtFlightTicket.Rows[i]["ExecutiveId"].ToString() : string.Empty;
                        model.Trip = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Trip"].ToString()) ? dtFlightTicket.Rows[i]["Trip"].ToString() : string.Empty;
                        model.RejectedRemark = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["RejectedRemark"].ToString()) ? dtFlightTicket.Rows[i]["RejectedRemark"].ToString() : string.Empty;
                        model.Provider = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Provider"].ToString()) ? dtFlightTicket.Rows[i]["Provider"].ToString() : string.Empty;
                        model.FareType = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["FTYPE"].ToString()) ? dtFlightTicket.Rows[i]["FTYPE"].ToString() : string.Empty;
                        model.PName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PName"].ToString()) ? dtFlightTicket.Rows[i]["PName"].ToString() : string.Empty;
                        model.JourneyDate = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["JourneyDate"].ToString()) ? dtFlightTicket.Rows[i]["JourneyDate"].ToString() : string.Empty;
                        model.PaymentMode = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PaymentMode"].ToString()) ? dtFlightTicket.Rows[i]["PaymentMode"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
        public static List<FlightTicketReportModel> GetFlightHoldTicketReports(FlightTicketFilter filter, ref int totalCount)
        {
            List<FlightTicketReportModel> result = new List<FlightTicketReportModel>();
            try
            {
                DataTable dtFlightTicket = GetHoldTicektReport(filter);
                totalCount = dtFlightTicket.Rows.Count;
                if (dtFlightTicket.Rows.Count > 0)
                {
                    for (int i = 0; i < dtFlightTicket.Rows.Count; i++)
                    {
                        FlightTicketReportModel model = new FlightTicketReportModel();
                        model.OrderId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["OrderId"].ToString()) ? dtFlightTicket.Rows[i]["OrderId"].ToString() : string.Empty;
                        model.TotalAfterDis = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["TotalAfterDis"].ToString()) ? Convert.ToDecimal(dtFlightTicket.Rows[i]["TotalAfterDis"].ToString()) : 0;
                        //model.TotalBookingCost = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["TotalBookingCost"].ToString()) ? Convert.ToDecimal(dtFlightTicket.Rows[i]["TotalBookingCost"].ToString()) : 0;
                        model.sector = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["sector"].ToString()) ? dtFlightTicket.Rows[i]["sector"].ToString() : string.Empty;
                        model.AgentId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgentId"].ToString()) ? dtFlightTicket.Rows[i]["AgentId"].ToString() : string.Empty;
                        model.AgencyName = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgencyName"].ToString()) ? dtFlightTicket.Rows[i]["AgencyName"].ToString() : string.Empty;
                        //model.AgentType = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AgentType"].ToString()) ? dtFlightTicket.Rows[i]["AgentType"].ToString() : string.Empty;
                        //model.PaxId = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["PaxId"].ToString()) ? Convert.ToInt32(dtFlightTicket.Rows[i]["PaxId"].ToString()) : 0;
                        model.Status = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["Status"].ToString()) ? dtFlightTicket.Rows[i]["Status"].ToString() : string.Empty;
                        model.CreateDate = dtFlightTicket.Rows[i]["CreateDate"].ToString();
                        model.VC = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["VC"].ToString()) ? dtFlightTicket.Rows[i]["VC"].ToString() : string.Empty;
                        model.GdsPnr = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["GdsPnr"].ToString()) ? dtFlightTicket.Rows[i]["GdsPnr"].ToString() : string.Empty;
                        model.AirlinePNR = !string.IsNullOrEmpty(dtFlightTicket.Rows[i]["AirlinePnr"].ToString()) ? dtFlightTicket.Rows[i]["AirlinePnr"].ToString() : string.Empty;
                        model.Adult = Convert.ToInt32(dtFlightTicket.Rows[i]["Adult"]) + Convert.ToInt32(dtFlightTicket.Rows[i]["Child"]) + Convert.ToInt32(dtFlightTicket.Rows[i]["Infant"]);
                        model.PgFName = dtFlightTicket.Rows[i]["PgFName"].ToString();
                        model.PgLName = dtFlightTicket.Rows[i]["PgLName"].ToString();
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
        public static List<FlightTicketReportModel> GetFixDepTicketReports(FlightTicketFilter filter, ref int totalCount)
        {
            //MM/dd/yyyy 12:00:00 AM
            return GetFlightTicketReports(filter, ref totalCount);
        }
        public static List<FlightTicketReportModel> GetFixDepSuppTicketReports(FlightTicketFilter filter, ref int totalCount)
        {
            //MM/dd/yyyy 12:00:00 AM
            return GetFlightSuppAminTicketReports(filter, ref totalCount);
        }
        public static List<FlightLedgerReportModel> GetFixDepLedgerReports(FlightLedgerFilter filter, ref int totalCount)
        {            
            return GetFlightLedgerReports(filter, ref totalCount);
        }

        public static List<FlightLedgerReportModel> GetFlightAllSuppliorLedgerReports(FlightLedgerFilter filter, ref int totalCount)
        {
            return GetFlightSuppliorLedgerReports(filter, ref totalCount);
        }

        public static List<FlightLedgerReportModel> GetSuppliorLedgerReports(FlightLedgerFilter filter, ref int totalCount)
        {
            return GetFlightLedgerReports(filter, ref totalCount);
        }
        public static bool InsertQuery(string queryKey)
        {
            try
            {
                command = new SqlCommand("SP_QueryKey", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@QueryKey", queryKey);
                command.Parameters.AddWithValue("@Action", "I");

                connection.Open();
                int isSuccess = command.ExecuteNonQuery();
                connection.Close();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static string ConvertStringDateToStringDateFormate(string date)
        {
            DateTime dtDate = new DateTime();

            if (!string.IsNullOrEmpty(date))
            {
                dtDate = DateTime.Parse(date);
                return dtDate.ToString("MM/dd/yyyy hh:mm tt");
            }

            return string.Empty;
        }

        public static DataTable GetsupplierReport(SupplierFilter filter)
        {
            try
            {
                command = new SqlCommand("AgencyDetailsForSupplier", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@FromDate", (!string.IsNullOrEmpty(filter.FromDate) ? filter.FromDate : string.Empty)));
                command.Parameters.Add(new SqlParameter("@ToDate", (!string.IsNullOrEmpty(filter.ToDate) ? filter.ToDate : string.Empty)));
                command.Parameters.Add(new SqlParameter("@UserId", (!string.IsNullOrEmpty(filter.UserId) ? filter.UserId : string.Empty)));
                command.Parameters.Add(new SqlParameter("@AgentType", (!string.IsNullOrEmpty(filter.AgentType) ? filter.AgentType : string.Empty)));
                command.Parameters.Add(new SqlParameter("@SalesExecID", (!string.IsNullOrEmpty(filter.SalesExecID) ? filter.SalesExecID : string.Empty)));
                command.Parameters.Add(new SqlParameter("@Email", (!string.IsNullOrEmpty(filter.Email) ? filter.Email : string.Empty)));
                command.Parameters.Add(new SqlParameter("@MobileNo", (!string.IsNullOrEmpty(filter.MobileNo) ? filter.MobileNo : string.Empty)));                
                objAdapter = new SqlDataAdapter();
                objAdapter.SelectCommand = command;
                objDS = new DataSet();
                objAdapter.Fill(objDS, "AgencyDetailsForSup");
                tempObjDT = objDS.Tables["AgencyDetailsForSup"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return tempObjDT;
        }

        public static List<SuppliorReportModel> GetsupplierReportList(SupplierFilter filter)
        {
            List<SuppliorReportModel> result = new List<SuppliorReportModel>();
            try
            {
                DataTable dtSupplior = GetsupplierReport(filter);                
                if (dtSupplior.Rows.Count > 0)
                {
                    for (int i = 0; i < dtSupplior.Rows.Count; i++)
                    {
                        SuppliorReportModel model = new SuppliorReportModel();
                        model.Counter=!string.IsNullOrEmpty(dtSupplior.Rows[i]["Counter"].ToString()) ? dtSupplior.Rows[i]["Counter"].ToString() : string.Empty;
                        model.User_Id = !string.IsNullOrEmpty(dtSupplior.Rows[i]["User_Id"].ToString()) ? dtSupplior.Rows[i]["User_Id"].ToString() : string.Empty;
                        model.AgencyId = !string.IsNullOrEmpty(dtSupplior.Rows[i]["AgencyId"].ToString()) ? dtSupplior.Rows[i]["AgencyId"].ToString() : string.Empty;
                        model.Agency_Name = !string.IsNullOrEmpty(dtSupplior.Rows[i]["Agency_Name"].ToString()) ? dtSupplior.Rows[i]["Agency_Name"].ToString() : string.Empty;
                        model.Balance = !string.IsNullOrEmpty(dtSupplior.Rows[i]["Balance"].ToString()) ? dtSupplior.Rows[i]["Balance"].ToString() : string.Empty;
                        model.DueAmount = !string.IsNullOrEmpty(dtSupplior.Rows[i]["DueAmount"].ToString()) ? dtSupplior.Rows[i]["DueAmount"].ToString() : string.Empty;
                        model.AgentLimit = !string.IsNullOrEmpty(dtSupplior.Rows[i]["AgentLimit"].ToString()) ? dtSupplior.Rows[i]["AgentLimit"].ToString() : string.Empty;
                        model.Crd_Trns_Date = !string.IsNullOrEmpty(dtSupplior.Rows[i]["Crd_Trns_Date"].ToString()) ? dtSupplior.Rows[i]["Crd_Trns_Date"].ToString() : string.Empty;
                        model.Mobile = !string.IsNullOrEmpty(dtSupplior.Rows[i]["Mobile"].ToString()) ? dtSupplior.Rows[i]["Mobile"].ToString() : string.Empty;
                        model.Email = !string.IsNullOrEmpty(dtSupplior.Rows[i]["Email"].ToString()) ? dtSupplior.Rows[i]["Email"].ToString() : string.Empty;
                        model.Agent_Type = !string.IsNullOrEmpty(dtSupplior.Rows[i]["Agent_Type"].ToString()) ? dtSupplior.Rows[i]["Agent_Type"].ToString() : string.Empty;                        
                        model.Name = !string.IsNullOrEmpty(dtSupplior.Rows[i]["Name"].ToString()) ? dtSupplior.Rows[i]["Name"].ToString() : string.Empty;
                        model.Address = !string.IsNullOrEmpty(dtSupplior.Rows[i]["Address"].ToString()) ? dtSupplior.Rows[i]["Address"].ToString() : string.Empty;
                        model.City = !string.IsNullOrEmpty(dtSupplior.Rows[i]["City"].ToString()) ? dtSupplior.Rows[i]["City"].ToString() : string.Empty;
                        model.State = !string.IsNullOrEmpty(dtSupplior.Rows[i]["State"].ToString()) ? dtSupplior.Rows[i]["State"].ToString() : string.Empty;
                        model.Country = !string.IsNullOrEmpty(dtSupplior.Rows[i]["Country"].ToString()) ? dtSupplior.Rows[i]["Country"].ToString() : string.Empty;
                        model.zipcode = !string.IsNullOrEmpty(dtSupplior.Rows[i]["zipcode"].ToString()) ? dtSupplior.Rows[i]["zipcode"].ToString() : string.Empty;
                        model.Phone = !string.IsNullOrEmpty(dtSupplior.Rows[i]["Phone"].ToString()) ? dtSupplior.Rows[i]["Phone"].ToString() : string.Empty;
                        model.PanNo = !string.IsNullOrEmpty(dtSupplior.Rows[i]["PanNo"].ToString()) ? dtSupplior.Rows[i]["PanNo"].ToString() : string.Empty;
                        model.GSTNO = !string.IsNullOrEmpty(dtSupplior.Rows[i]["GSTNO"].ToString()) ? dtSupplior.Rows[i]["GSTNO"].ToString() : string.Empty;
                        model.SalesExecID = !string.IsNullOrEmpty(dtSupplior.Rows[i]["SalesExecID"].ToString()) ? dtSupplior.Rows[i]["SalesExecID"].ToString() : string.Empty;                        
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;

        }
    }
}