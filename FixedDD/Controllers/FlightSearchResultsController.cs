﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using System.Web.UI;
using FixedDD.Helper;
using FixedDD.Models;
using System.Net;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace FixedDD.Controllers
{
    public class FlightSearchResultsController : Controller
    {
        private myAmdDB db = new myAmdDB();

        public Page Page { get; private set; }

        public ActionResult SupplierList(SupplierFilter filter)
        {

            SuppliorReportModel model = new SuppliorReportModel();
            try
            {
                string stdate = "";
                string enddate = "";
                filter.AgentType = string.Empty;
                if (!string.IsNullOrEmpty(filter.FromDate))
                {
                    stdate = filter.FromDate.Split('/')[2] + "/" + filter.FromDate.Split('/')[1] + "/" + filter.FromDate.Split('/')[0];
                    filter.FromDate = stdate + " 12:00:00 AM";
                }
                if (!string.IsNullOrEmpty(filter.ToDate))
                {
                    enddate = filter.ToDate.Split('/')[2] + "/" + filter.ToDate.Split('/')[1] + "/" + filter.ToDate.Split('/')[0];
                    filter.ToDate = enddate + " 11:59:59 PM";
                }
                model.SuppliorReportlist = AccountHelper.GetsupplierReportList(filter);
                model.Totalcount = model.SuppliorReportlist.Count();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        public ActionResult SupplierPnr(string from, string to, string travelDatestart, string travelDateEnd, string supplierid, string Pnr, string AirLine, string flightno)
        {
            string matchkey = "";
            List<FlightSearchResults> stdList;
            int totalskip;
            string UserID = "";
            string pwd = "";
            string R1 = "";
            string rr = "";

            bool no = false;


            if (Request.QueryString["R1"] != null)
            {
                if (AccountHelper.InsertQuery(Request.QueryString["R1"].ToString()))
                {
                    matchkey = QueryKeyI(Request.QueryString["R1"]);
                }
            }

            //  Response.Write("<script>alert('" + rr + "');</script>");



            if (matchkey == "NO")
            {
                no = true;

                if (Convert.ToString(Session["query"]) != null)
                {
                    try
                    {
                        if (Request.QueryString["R1"] != null && no == false)
                        {

                            if (Convert.ToString(Session["R1"]) == Convert.ToString(Request.QueryString["R1"]))
                            {
                                R1 = Session["R1"].ToString();
                                UserID = R1.Split('-')[0];
                                pwd = R1.Split('-')[1];
                            }
                            else
                            {
                                R1 = Decrypt(Request.QueryString["R1"].ToString());
                                Session["R1"] = R1;
                                UserID = R1.Split('-')[0];
                                pwd = R1.Split('-')[1];
                            }

                        }
                        else if (Request.QueryString["R1"] == null && no == false)
                        {
                            if (Convert.ToString(Session["R1"]) == "")
                            {
                                //Redirect Action
                            }
                            else
                            {
                                R1 = Session["R1"].ToString();
                                UserID = R1.Split('-')[0];
                                pwd = R1.Split('-')[1];
                            }
                        }
                        else
                        {
                            // UserID = Decrypt(Request.QueryString["UserID"].ToString());
                            //pwd = Decrypt(Request.QueryString["Code"].ToString());
                        }




                        int temp = validUser(UserID, pwd);

                        if (Session["isExecutive"] != null)
                        {
                            temp = 1;
                        }

                        if (temp > 0)
                        {
                            if (Convert.ToString(Session["R1"]) == "")
                            {
                                Session["userid"] = UserID;
                                Session["Code"] = pwd;
                                Session["R1"] = UserID + "-" + pwd;
                                Session["query"] = UserID;
                                return RedirectToAction("Sector");
                            }
                            else
                            {
                                Session["userid"] = UserID;
                                Session["Code"] = pwd;

                            }




                        }
                        else
                        {

                            Response.Redirect("/");

                        }

                    }
                    catch
                    {
                        Response.Redirect("/");

                    }

                }
                else
                {
                    Response.Redirect("/");
                }
            }
            else if (matchkey == "YES" || Convert.ToString(Session["query"]) != null)
            {


                try
                {
                    if (Request.QueryString["R1"] != null && no == false)
                    {

                        if (Convert.ToString(Session["R1"]) == Convert.ToString(Request.QueryString["R1"]))
                        {
                            R1 = Session["R1"].ToString();
                            UserID = R1.Split('-')[0];
                            pwd = R1.Split('-')[1];
                        }
                        else
                        {
                            R1 = Decrypt(Request.QueryString["R1"].ToString());
                            Session["R1"] = R1;
                            UserID = R1.Split('-')[0];
                            pwd = R1.Split('-')[1];
                        }

                    }
                    else if (Request.QueryString["R1"] == null && no == false)
                    {
                        if (Convert.ToString(Session["R1"]) == "")
                        {
                            //Redirect Action
                        }
                        else
                        {
                            R1 = Session["R1"].ToString();
                            UserID = R1.Split('-')[0];
                            pwd = R1.Split('-')[1];
                        }
                    }
                    else
                    {
                        // UserID = Decrypt(Request.QueryString["UserID"].ToString());
                        //pwd = Decrypt(Request.QueryString["Code"].ToString());
                    }




                    int temp = validUser(UserID, pwd);
                    if (Session["isExecutive"] != null)
                    {
                        temp = 1;
                    }
                    if (temp > 0)
                    {
                        if (Convert.ToString(Session["R1"]) == "")
                        {
                            Session["userid"] = UserID;
                            Session["Code"] = pwd;
                            Session["R1"] = UserID + "-" + pwd;
                            Session["query"] = UserID;
                            return RedirectToAction("Sector");
                        }
                        else
                        {
                            Session["userid"] = UserID;
                            Session["Code"] = pwd;

                        }




                    }
                    else
                    {

                        Response.Redirect("/");

                    }

                }
                catch
                {
                    Response.Redirect("/");

                }
            }

            GetSupplierIndex(from, to, travelDatestart, travelDateEnd, supplierid, Pnr, AirLine, flightno, out stdList);
            return View(stdList);
        }
        public ActionResult SupplierTicketReport(FlightTicketFilter filter, string submitButton)
        {
            FlightTicketReportModel model = new FlightTicketReportModel();
            try
            {
                int totalcount = 0;
                filter.Status = StatusClass.Ticketed.ToString();
                filter.UserType = "SuppAdmin";
                filter.Suppliorid = Session["userid"].ToString();
                string stdate = "";
                string enddate = "";
                if (!string.IsNullOrEmpty(filter.FromDate))
                {
                    stdate = filter.FromDate.Split('/')[2] + "/" + filter.FromDate.Split('/')[1] + "/" + filter.FromDate.Split('/')[0];
                    filter.FromDate = stdate + " 12:00:00 AM";
                }
                if (!string.IsNullOrEmpty(filter.ToDate))
                {
                    enddate = filter.ToDate.Split('/')[2] + "/" + filter.ToDate.Split('/')[1] + "/" + filter.ToDate.Split('/')[0];
                    filter.ToDate = enddate + " 11:59:59 PM";
                }
                model.TicketReportlist = AccountHelper.GetFixDepSuppTicketReports(filter, ref totalcount);
                if (submitButton == "EXPORT")
                {
                    DataSet tempDS = AccountHelper.GetTicektReportSuppAdmin(filter);
                    if (tempDS != null)
                    {
                        DataTable dtLedDetail = tempDS.Tables[0];

                        DataTable dt = new DataTable();
                        dt.Clear();
                        dt.Columns.Add("SUPPLIER ID");
                        dt.Columns.Add("ORDER ID");
                        dt.Columns.Add("PNR");
                        dt.Columns.Add("TICKET NO");
                        dt.Columns.Add("PAX NAME");
                        dt.Columns.Add("AIRLINE");
                        dt.Columns.Add("SECTOR");
                        dt.Columns.Add("BOOKING DATE");
                        dt.Columns.Add("JOURNEY DATE");
                        dt.Columns.Add("AMOUNT");

                        if (dtLedDetail != null && dtLedDetail.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtLedDetail.Rows.Count; i++)
                            {
                                DataRow _ledger = dt.NewRow();
                                _ledger["SUPPLIER ID"] = dtLedDetail.Rows[i]["SearchId"].ToString();
                                _ledger["ORDER ID"] = dtLedDetail.Rows[i]["OrderId"].ToString();
                                _ledger["PNR"] = dtLedDetail.Rows[i]["GdsPnr"].ToString();
                                _ledger["TICKET NO"] = dtLedDetail.Rows[i]["TicketNumber"].ToString();
                                _ledger["PAX NAME"] = dtLedDetail.Rows[i]["PName"].ToString();
                                _ledger["AIRLINE"] = dtLedDetail.Rows[i]["VC"].ToString();
                                _ledger["SECTOR"] = dtLedDetail.Rows[i]["sector"].ToString();
                                _ledger["BOOKING DATE"] = dtLedDetail.Rows[i]["CreateDate"].ToString();
                                _ledger["JOURNEY DATE"] = dtLedDetail.Rows[i]["JourneyDate"].ToString();
                                _ledger["AMOUNT"] = dtLedDetail.Rows[i]["TotalAfterDis"].ToString();
                                dt.Rows.Add(_ledger);
                            }
                        }

                        DataSet dsNelwLedDel = new DataSet();
                        dsNelwLedDel.Tables.Add(dt);
                        Global.ExportData(dsNelwLedDel);
                    }
                }

                model.Totalcount = totalcount;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        public ActionResult AddOnlySeat(string id, string seat2, string totalst, string pnr)
        {
            FixdDepModel model = new FixdDepModel();
            if (!string.IsNullOrEmpty(seat2) && !string.IsNullOrEmpty(totalst) && !string.IsNullOrEmpty(id))
            {
                if (Session["userid"] != null)
                {
                    if (!string.IsNullOrEmpty(id))
                    {
                        List<FlightSearchResults> stdList = new List<FlightSearchResults>();
                        if (!Convert.ToBoolean(Session["IsSupplierAdmin"].ToString()))
                        {
                            string loggedin_userid = Session["userid"].ToString();
                            stdList = db.FlightSearchResults.Where(p => p.fareBasis == pnr && p.Avl_Seat != "0" && p.CreatedByUserid == loggedin_userid).OrderBy(p => p.id).ToList();
                        }
                        else
                        {
                            stdList = db.FlightSearchResults.Where(p => p.fareBasis == pnr && p.Avl_Seat != "0").OrderBy(p => p.id).ToList();
                        }

                        List<SelectListItem> listItems = new List<SelectListItem>();
                        if (stdList != null && stdList.Count > 0)
                        {
                            foreach (var item in stdList)
                            {
                                listItems.Add(new SelectListItem
                                {
                                    Text = item.Grand_Total.ToString() + "," + item.Avl_Seat,
                                    Value = item.id.ToString()
                                });
                            }
                        }
                        model.BasicFareList = listItems;
                    }
                    model.Pnr = pnr;
                    model.Id = id;
                    model.Avl_Seat = seat2;
                    model.Total_Seats = totalst;
                }
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult AddOnlySeat(FixdDepModel model, string submitButton)
        {
            int toltalseat = 0;
            int avlseat = 0;

            if (!string.IsNullOrEmpty(model.Id))
            {
                int thisid = Convert.ToInt32(model.Id);
                FlightSearchResults singleDetail = db.FlightSearchResults.Where(p => p.id == thisid).FirstOrDefault();

                if (singleDetail != null && singleDetail.id > 0)
                {
                    FlightSearchResults newflightDel = new FlightSearchResults();

                    if (submitButton == "Add Seat")
                    {
                        var isEmptyGrandValue = db.FlightSearchResults.Where(p => p.fareBasis == singleDetail.fareBasis && p.Grand_Total == 0).FirstOrDefault();

                        if (isEmptyGrandValue == null)
                        {
                            newflightDel.Departure_Date = singleDetail.Departure_Date;
                            newflightDel.Arrival_Date = singleDetail.Arrival_Date;
                            newflightDel.Triptype = singleDetail.Triptype;
                            newflightDel.DepartureTerminal = singleDetail.DepartureTerminal;
                            newflightDel.ArrivalTerminal = singleDetail.ArrivalTerminal;
                            newflightDel.DepartureTime = singleDetail.DepartureTime;
                            newflightDel.ArrivalTime = singleDetail.ArrivalTime;
                            newflightDel.BagInfo = singleDetail.BagInfo;
                            newflightDel.FareRule = singleDetail.FareRule;
                            newflightDel.FareDet = singleDetail.FareDet;
                            newflightDel.FlightNo = singleDetail.FlightNo;
                            newflightDel.ClassType = singleDetail.ClassType;
                            newflightDel.OrgDestFrom = singleDetail.OrgDestFrom;
                            newflightDel.OrgDestTo = singleDetail.OrgDestTo;
                            newflightDel.AirLineName = singleDetail.AirLineName;
                            newflightDel.Rt_AirLineName = singleDetail.Rt_AirLineName;
                            newflightDel.Grand_Total = 0;//set
                            newflightDel.DepAirportCode = singleDetail.DepAirportCode;
                            newflightDel.DepartureCityName = singleDetail.DepartureCityName;
                            newflightDel.DepartureAirportName = singleDetail.DepartureAirportName;
                            newflightDel.ArrivalCityName = singleDetail.ArrivalCityName;
                            newflightDel.ArrAirportCode = singleDetail.ArrAirportCode;
                            newflightDel.ArrivalAirportName = singleDetail.ArrivalAirportName;
                            newflightDel.MarketingCarrier = singleDetail.MarketingCarrier;
                            newflightDel.valid_Till = singleDetail.valid_Till;
                            newflightDel.RBD = singleDetail.RBD;
                            newflightDel.Total_Seats = model.Seat;//added seat
                            newflightDel.Used_Seat = "0";
                            newflightDel.Basicfare = 0;
                            newflightDel.YQ = 0;
                            newflightDel.YR = 0;
                            newflightDel.WO = 0;
                            newflightDel.OT = 0;
                            newflightDel.GROSS_Total = 0;
                            newflightDel.Status = true;

                            newflightDel.InfFare = 0;
                            newflightDel.infBfare = 0;

                            newflightDel.MarkupWithGst = "0";
                            newflightDel.SupMarkup = "0";
                            newflightDel.Markup_Type = singleDetail.Markup_Type;
                            newflightDel.Admin_Markup = 0;
                            newflightDel.TempStatus = singleDetail.TempStatus;
                            newflightDel.Markup = 0;
                            newflightDel.Avl_Seat = model.Seat;
                            newflightDel.fareBasis = singleDetail.fareBasis;
                            newflightDel.FixedDepStatus = singleDetail.FixedDepStatus;
                            newflightDel.CreatedByUserid = singleDetail.CreatedByUserid;
                            newflightDel.CreatedByName = singleDetail.CreatedByName;
                            newflightDel.IsPrimary = false;
                            db.FlightSearchResults.Add(newflightDel);
                            db.SaveChanges();

                            int createid = newflightDel.id;
                            if (createid > 0)
                            {
                                DataTable dt = new DataTable();
                                DataSet ds = new DataSet();
                                SqlDataAdapter adp = new SqlDataAdapter();
                                string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
                                SqlConnection con = new SqlConnection(constr);
                                SqlCommand cmd = new SqlCommand("FixDepatureCreate");
                                cmd.Connection = con;
                                con.Open();
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@createid", createid);
                                adp.SelectCommand = cmd;
                                int temp = adp.Fill(ds);
                                cmd.Dispose();

                                TempData["Msg"] = "Seat added successfully";
                            }
                        }
                        else
                        {
                            string fieldswithvalue = "Total_Seats=" + (Convert.ToInt32(isEmptyGrandValue.Total_Seats) + Convert.ToInt32(model.Seat)) + ",Avl_Seat = " + (Convert.ToInt32(isEmptyGrandValue.Avl_Seat) + Convert.ToInt32(model.Seat));
                            string tablename = "FlightSearchResults";
                            string wherecondition = "Id = " + isEmptyGrandValue.id;
                            bool IsSuccess = UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition);
                            if (IsSuccess)
                            {
                                TempData["Msg"] = "Seat added successfully";
                            }
                        }
                    }
                    else
                    {

                    }
                }
            }
            return View(model);
        }
        public ActionResult AddSeat(string id, string seat2, string totalst, string pnr)
        {
            FixdDepModel model = new FixdDepModel();
            if (!string.IsNullOrEmpty(seat2) && !string.IsNullOrEmpty(totalst) && !string.IsNullOrEmpty(id))
            {
                if (Session["userid"] != null)
                {
                    if (!string.IsNullOrEmpty(id))
                    {
                        List<FlightSearchResults> stdList = new List<FlightSearchResults>();
                        if (!Convert.ToBoolean(Session["IsSupplierAdmin"].ToString()))
                        {
                            string loggedin_userid = Session["userid"].ToString();//&& p.Avl_Seat != "0"
                            stdList = db.FlightSearchResults.Where(p => p.fareBasis == pnr && p.CreatedByUserid == loggedin_userid).OrderBy(p => p.id).ToList();
                        }
                        else
                        {
                            stdList = db.FlightSearchResults.Where(p => p.fareBasis == pnr).OrderBy(p => p.id).ToList();
                        }

                        List<SelectListItem> listItems = new List<SelectListItem>();
                        if (stdList != null && stdList.Count > 0)
                        {
                            foreach (var item in stdList)
                            {
                                listItems.Add(new SelectListItem
                                {
                                    Text = item.Grand_Total.ToString() + "," + item.Avl_Seat,
                                    Value = item.id.ToString()
                                });
                            }
                        }
                        model.BasicFareList = listItems;
                    }
                    model.Pnr = pnr;
                    model.Id = id;
                    model.Avl_Seat = seat2;
                    model.Total_Seats = totalst;
                }
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult AddSeat(FixdDepModel model, string submitButton)
        {
            int toltalseat = 0;
            int avlseat = 0;

            if (!string.IsNullOrEmpty(model.Id))
            {
                int thisid = Convert.ToInt32(model.Id);
                FlightSearchResults singleDetail = db.FlightSearchResults.Where(p => p.id == thisid).FirstOrDefault();

                if (singleDetail != null && singleDetail.id > 0)
                {
                    FlightSearchResults newflightDel = new FlightSearchResults();

                    if (submitButton == "Add Seat")
                    {
                        var isEmptyGrandValue = db.FlightSearchResults.Where(p => p.fareBasis == singleDetail.fareBasis && p.Grand_Total == 0).FirstOrDefault();

                        if (isEmptyGrandValue == null)
                        {
                            newflightDel.Departure_Date = singleDetail.Departure_Date;
                            newflightDel.Arrival_Date = singleDetail.Arrival_Date;
                            newflightDel.Triptype = singleDetail.Triptype;
                            newflightDel.DepartureTerminal = singleDetail.DepartureTerminal;
                            newflightDel.ArrivalTerminal = singleDetail.ArrivalTerminal;
                            newflightDel.DepartureTime = singleDetail.DepartureTime;
                            newflightDel.ArrivalTime = singleDetail.ArrivalTime;
                            newflightDel.BagInfo = singleDetail.BagInfo;
                            newflightDel.FareRule = singleDetail.FareRule;
                            newflightDel.FareDet = singleDetail.FareDet;
                            newflightDel.FlightNo = singleDetail.FlightNo;
                            newflightDel.ClassType = singleDetail.ClassType;
                            newflightDel.OrgDestFrom = singleDetail.OrgDestFrom;
                            newflightDel.OrgDestTo = singleDetail.OrgDestTo;
                            newflightDel.AirLineName = singleDetail.AirLineName;
                            newflightDel.Rt_AirLineName = singleDetail.Rt_AirLineName;
                            newflightDel.Grand_Total = 0;//set
                            newflightDel.DepAirportCode = singleDetail.DepAirportCode;
                            newflightDel.DepartureCityName = singleDetail.DepartureCityName;
                            newflightDel.DepartureAirportName = singleDetail.DepartureAirportName;
                            newflightDel.ArrivalCityName = singleDetail.ArrivalCityName;
                            newflightDel.ArrAirportCode = singleDetail.ArrAirportCode;
                            newflightDel.ArrivalAirportName = singleDetail.ArrivalAirportName;
                            newflightDel.MarketingCarrier = singleDetail.MarketingCarrier;
                            newflightDel.valid_Till = singleDetail.valid_Till;
                            newflightDel.RBD = singleDetail.RBD;
                            newflightDel.Total_Seats = model.Seat;//added seat
                            newflightDel.Used_Seat = "0";
                            newflightDel.Basicfare = 0;
                            newflightDel.YQ = 0;
                            newflightDel.YR = 0;
                            newflightDel.WO = 0;
                            newflightDel.OT = 0;
                            newflightDel.GROSS_Total = 0;
                            newflightDel.Status = true;

                            newflightDel.InfFare = 0;
                            newflightDel.infBfare = 0;

                            newflightDel.MarkupWithGst = "0";
                            newflightDel.SupMarkup = "0";
                            newflightDel.Markup_Type = singleDetail.Markup_Type;
                            newflightDel.Admin_Markup = 0;
                            newflightDel.TempStatus = singleDetail.TempStatus;
                            newflightDel.Markup = 0;
                            newflightDel.Avl_Seat = model.Seat;
                            newflightDel.fareBasis = singleDetail.fareBasis;
                            newflightDel.FixedDepStatus = singleDetail.FixedDepStatus;
                            newflightDel.CreatedByUserid = singleDetail.CreatedByUserid;
                            newflightDel.CreatedByName = singleDetail.CreatedByName;
                            newflightDel.IsPrimary = false;
                            db.FlightSearchResults.Add(newflightDel);
                            db.SaveChanges();

                            int createid = newflightDel.id;
                            if (createid > 0)
                            {
                                DataTable dt = new DataTable();
                                DataSet ds = new DataSet();
                                SqlDataAdapter adp = new SqlDataAdapter();
                                string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
                                SqlConnection con = new SqlConnection(constr);
                                SqlCommand cmd = new SqlCommand("FixDepatureCreate");
                                cmd.Connection = con;
                                con.Open();
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@createid", createid);
                                adp.SelectCommand = cmd;
                                int temp = adp.Fill(ds);
                                cmd.Dispose();

                                TempData["Msg"] = "Seat added successfully";
                            }
                        }
                        else
                        {
                            string fieldswithvalue = "Total_Seats=" + (Convert.ToInt32(isEmptyGrandValue.Total_Seats) + Convert.ToInt32(model.Seat)) + ",Avl_Seat = " + (Convert.ToInt32(isEmptyGrandValue.Avl_Seat) + Convert.ToInt32(model.Seat));
                            string tablename = "FlightSearchResults";
                            string wherecondition = "Id = " + isEmptyGrandValue.id;
                            bool IsSuccess = UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition);
                            if (IsSuccess)
                            {
                                TempData["Msg"] = "Seat added successfully";
                            }
                        }
                    }
                    else
                    {

                    }
                }
            }




            //if (submitButton == "Add Seat")
            //{
            //    toltalseat = Convert.ToInt32(model.Total_Seats) + Convert.ToInt32(model.Seat);
            //    avlseat = Convert.ToInt32(model.Avl_Seat) + Convert.ToInt32(model.Seat);
            //}
            //else
            //{
            //    toltalseat = Convert.ToInt32(model.Total_Seats) - Convert.ToInt32(model.Seat);
            //    avlseat = Convert.ToInt32(model.Avl_Seat) - Convert.ToInt32(model.Seat);
            //}
            //string fieldswithvalue = "Avl_Seat = " + avlseat + " ,Total_Seats=" + toltalseat;
            //string tablename = "FlightSearchResults";
            //string wherecondition = "Id = " + model.Id;
            //bool IsSuccess = UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition);
            //if (IsSuccess)
            //{
            //if (submitButton == "Add Seat")
            //{
            //    TempData["Msg"] = "Seat added successfully";
            //}
            //else
            //{
            //    TempData["Msg"] = "Seat deducted successfully";
            //}
            // }

            return View(model);
        }

        #region [New Update Add Seat]        
        public JsonResult AddNewSeat(FixdDepModel model)
        {
            string message = string.Empty;
            if (AddNewSeat_Update(model) == true)
            {
                message = "Seat added successfully";
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }
        private bool AddNewSeat_Update(FixdDepModel model)
        {
            bool isAdded = false;
            if (model.Id != null)
            {
                try
                {
                    if (Session["userid"] != null)
                    {
                        int thisid = Convert.ToInt32(model.Id);

                        FlightSearchResults singleDetail = new FlightSearchResults();
                        if (!Convert.ToBoolean(Session["IsSupplierAdmin"].ToString()))
                        {
                            string loggedin_userid = Session["userid"].ToString();
                            singleDetail = db.FlightSearchResults.Where(p => p.id == thisid && p.CreatedByUserid == loggedin_userid).FirstOrDefault();
                        }
                        else
                        {
                            singleDetail = db.FlightSearchResults.Where(p => p.id == thisid).FirstOrDefault();
                        }

                        if (singleDetail != null && singleDetail.id > 0)
                        {
                            FlightSearchResults newflightDel = new FlightSearchResults();

                            newflightDel.Departure_Date = singleDetail.Departure_Date;
                            newflightDel.Arrival_Date = singleDetail.Arrival_Date;
                            newflightDel.Triptype = singleDetail.Triptype;
                            newflightDel.DepartureTerminal = singleDetail.DepartureTerminal;
                            newflightDel.ArrivalTerminal = singleDetail.ArrivalTerminal;
                            newflightDel.DepartureTime = singleDetail.DepartureTime;
                            newflightDel.ArrivalTime = singleDetail.ArrivalTime;
                            newflightDel.BagInfo = singleDetail.BagInfo;
                            newflightDel.FareRule = singleDetail.FareRule;
                            newflightDel.FareDet = singleDetail.FareDet;
                            newflightDel.FlightNo = singleDetail.FlightNo;
                            newflightDel.ClassType = singleDetail.ClassType;
                            newflightDel.OrgDestFrom = singleDetail.OrgDestFrom;
                            newflightDel.OrgDestTo = singleDetail.OrgDestTo;
                            newflightDel.AirLineName = singleDetail.AirLineName;
                            newflightDel.Rt_AirLineName = singleDetail.Rt_AirLineName;
                            newflightDel.Grand_Total = Convert.ToInt32(model.Grand_total);//set
                            newflightDel.DepAirportCode = singleDetail.DepAirportCode;
                            newflightDel.DepartureCityName = singleDetail.DepartureCityName;
                            newflightDel.DepartureAirportName = singleDetail.DepartureAirportName;
                            newflightDel.ArrivalCityName = singleDetail.ArrivalCityName;
                            newflightDel.ArrAirportCode = singleDetail.ArrAirportCode;
                            newflightDel.ArrivalAirportName = singleDetail.ArrivalAirportName;
                            newflightDel.MarketingCarrier = singleDetail.MarketingCarrier;
                            newflightDel.valid_Till = singleDetail.valid_Till;
                            newflightDel.RBD = singleDetail.RBD;
                            newflightDel.Total_Seats = model.Seat;//added seat
                            newflightDel.Used_Seat = "0";
                            newflightDel.Basicfare = Convert.ToInt32(model.Basefare);
                            newflightDel.YQ = Convert.ToInt32(model.YQ);
                            newflightDel.YR = Convert.ToInt32(model.YR);
                            newflightDel.WO = Convert.ToInt32(model.WO);
                            newflightDel.OT = Convert.ToInt32(model.OT);
                            newflightDel.GROSS_Total = Convert.ToInt32(model.Grand_total);
                            newflightDel.Status = true;

                            newflightDel.InfFare = Convert.ToInt32(model.Infentfare);
                            newflightDel.infBfare = Convert.ToInt32(model.Infentfare);

                            newflightDel.MarkupWithGst = "0";
                            newflightDel.SupMarkup = "0";
                            newflightDel.Markup_Type = singleDetail.Markup_Type;
                            newflightDel.Admin_Markup = 0;
                            newflightDel.TempStatus = singleDetail.TempStatus;
                            newflightDel.Markup = 0;
                            newflightDel.Avl_Seat = model.Seat;
                            newflightDel.fareBasis = singleDetail.fareBasis;
                            newflightDel.FixedDepStatus = true;
                            newflightDel.CreatedByUserid = singleDetail.CreatedByUserid;
                            newflightDel.CreatedByName = singleDetail.CreatedByName;
                            newflightDel.IsPrimary = false;
                            db.FlightSearchResults.Add(newflightDel);
                            db.SaveChanges();

                            int createid = newflightDel.id;
                            if (createid > 0)
                            {
                                DataTable dt = new DataTable();
                                DataSet ds = new DataSet();
                                SqlDataAdapter adp = new SqlDataAdapter();
                                string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
                                SqlConnection con = new SqlConnection(constr);
                                SqlCommand cmd = new SqlCommand("FixDepatureCreate");
                                cmd.Connection = con;
                                con.Open();
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@createid", createid);
                                adp.SelectCommand = cmd;
                                int temp = adp.Fill(ds);
                                cmd.Dispose();

                                isAdded = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
            }
            return isAdded;
        }
        public JsonResult MinusNewSeat(string id, string pnr, string seat)
        {
            string message = string.Empty;
            if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(pnr) && !string.IsNullOrEmpty(seat))
            {
                if (MinusNewSeat_Update(id, pnr, seat) == true)
                {
                    message = "Seat minus successfully";
                }
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }
        private bool MinusNewSeat_Update(string id, string pnr, string seat)
        {
            bool isMinus = false;
            try
            {
                if (Session["userid"] != null)
                {
                    string loggedin_userid = string.Empty;
                    if (!Convert.ToBoolean(Session["IsSupplierAdmin"].ToString()))
                    {
                        loggedin_userid = Session["userid"].ToString();
                    }

                    int thisid = Convert.ToInt32(id);
                    FlightSearchResults singleDetail = db.FlightSearchResults.Where(p => p.id == thisid).FirstOrDefault();
                    if (singleDetail != null && singleDetail.id > 0)
                    {
                        int avlseat = Convert.ToInt32(singleDetail.Avl_Seat) - Convert.ToInt32(seat);
                        int totseat = Convert.ToInt32(singleDetail.Total_Seats) - Convert.ToInt32(seat);

                        string fieldswithvalue = "Avl_Seat = " + avlseat + ",Total_Seats=" + totseat;
                        string tablename = "FlightSearchResults";
                        string wherecondition = "Id = " + thisid + "";

                        if (!string.IsNullOrEmpty(loggedin_userid))
                        {
                            wherecondition += " and CreatedByUserid='" + loggedin_userid + "'";
                        }

                        bool IsSuccess = UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition);
                        if (IsSuccess)
                        {
                            isMinus = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return isMinus;
        }
        #endregion

        public ActionResult Markup2(string fddid, string aircode, string org, string dest, string flightno)
        {
            MarkupModel model = new MarkupModel();
            if (!string.IsNullOrEmpty(fddid))
            {
                model.FddId = Convert.ToInt32(fddid);
                model.Airline = aircode.Replace("(", "").Replace(")", "");
                model.Org = org.Replace("(", "").Replace(")", "");
                model.Dest = dest.Replace("(", "").Replace(")", "");
                model.FlightNo = flightno;
                string fileds = "*";
                string tablename = "MISC_SRV_CHARGE";
                string whereCondition = "FddId=" + model.FddId + "";
                DataTable map = GetRecordFromTable(fileds, tablename, whereCondition);
                if (map.Rows.Count > 0)
                {
                    model.Airline = !string.IsNullOrEmpty(map.Rows[0]["Airline"].ToString()) ? map.Rows[0]["Airline"].ToString() : string.Empty;
                    model.Trip = !string.IsNullOrEmpty(map.Rows[0]["Trip"].ToString()) ? map.Rows[0]["Trip"].ToString() : string.Empty;
                    model.Amount = map.Rows[0]["Amount"].ToString();
                    model.CommisionOnBasic = map.Rows[0]["CommisionOnBasic"].ToString();
                    model.GroupType = !string.IsNullOrEmpty(map.Rows[0]["GroupType"].ToString()) ? map.Rows[0]["GroupType"].ToString() : string.Empty;
                    model.Org = !string.IsNullOrEmpty(map.Rows[0]["Org"].ToString()) ? map.Rows[0]["Org"].ToString() : string.Empty;
                    model.Dest = !string.IsNullOrEmpty(map.Rows[0]["Dest"].ToString()) ? map.Rows[0]["Dest"].ToString() : string.Empty;
                    model.CommissionOnYq = !string.IsNullOrEmpty(map.Rows[0]["CommissionOnYq"].ToString()) ? map.Rows[0]["CommissionOnYq"].ToString() : string.Empty;
                    model.CommisionOnBasicYq = !string.IsNullOrEmpty(map.Rows[0]["CommisionOnBasicYq"].ToString()) ? map.Rows[0]["CommisionOnBasicYq"].ToString() : string.Empty;
                    model.MarkupType = !string.IsNullOrEmpty(map.Rows[0]["MarkupType"].ToString()) ? map.Rows[0]["MarkupType"].ToString() : string.Empty;
                    model.Counter = !string.IsNullOrEmpty(map.Rows[0]["Counter"].ToString()) ? map.Rows[0]["Counter"].ToString() : string.Empty;

                }
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Markup2(MarkupModel model)
        {
            if (string.IsNullOrEmpty(model.Counter))
            {
                model.CreatedBy = Session["userid"].ToString();
                bool IsSuccess = INSERTMISCSRV_SUPPLIORWithGST(model);
                if (IsSuccess)
                {
                    TempData["Msg"] = "Markup2 added successfully";

                }
            }
            else
            {
                string fieldswithvalue = "MarkupType = '" + model.MarkupType + "',Amount = " + model.Amount + ",CommisionOnBasic = " + model.CommisionOnBasic + ",CommissionOnYq =" + model.CommissionOnYq + " ,CommisionOnBasicYq =" + model.CommisionOnBasicYq + "";
                string tablename = "MISC_SRV_CHARGE";
                string wherecondition = "Counter = " + model.Counter;
                bool Isupdated = UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition);
                if (Isupdated)
                {
                    TempData["Msg"] = "Markup2 Updated successfully";
                }
            }

            return View(model);
        }

        public ActionResult ViewFare(string id, string pnr)
        {
            FixedViewFare model = new FixedViewFare();

            if (!string.IsNullOrEmpty(pnr))
            {
                if (Session["userid"] != null)
                {
                    List<FlightSearchResults> stdList = new List<FlightSearchResults>();
                    if (!Convert.ToBoolean(Session["IsSupplierAdmin"].ToString()))
                    {
                        string loggedin_userid = Session["userid"].ToString();
                        stdList = db.FlightSearchResults.Where(p => p.fareBasis == pnr && p.CreatedByUserid == loggedin_userid).OrderBy(p => p.id).ToList();
                    }
                    else
                    {
                        stdList = db.FlightSearchResults.Where(p => p.fareBasis == pnr).OrderBy(p => p.id).ToList();
                    }

                    if (stdList != null && stdList.Count > 0)
                    {
                        List<FixedViewFare> fareList = new List<FixedViewFare>();
                        foreach (var item in stdList)
                        {
                            model = new FixedViewFare();
                            model.Amount = item.Grand_Total.ToString();
                            model.SoldSeat = item.Used_Seat.ToString();
                            model.UnSoldSeat = item.Avl_Seat.ToString();

                            fareList.Add(model);
                        }
                        model.FixedViewFareList = fareList;
                    }
                }
                else
                {
                    Response.Redirect("/");
                }
            }
            return View(model);
        }

        public ActionResult AddFare(string id, string pnr)
        {
            FixdDepModel model = new FixdDepModel();

            if (!string.IsNullOrEmpty(id))
            {
                //int thisid = Convert.ToInt32(id);
                List<FlightSearchResults> stdList = db.FlightSearchResults.Where(p => p.fareBasis == pnr && p.Grand_Total == 0 && p.Avl_Seat != "0").ToList();
                if (stdList != null && stdList.Count > 0)
                {
                    model.Id = id;
                    model.Pnr = pnr;
                    model.Total_Seats = stdList[0].Total_Seats;
                    model.Avl_Seat = stdList[0].Avl_Seat;

                    model.NoOfSeatApply = "0";
                    model.Infentfare = "0";
                    model.Basefare = "0";
                    model.YQ = "0";
                    model.YR = "0";
                    model.OT = "0";
                    model.Amount = "0";
                    model.Markup = "0";
                    model.Markup2 = "0";
                }
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult AddFare(FixdDepModel model)
        {
            if (!string.IsNullOrEmpty(model.Id))
            {
                int thisid = Convert.ToInt32(model.Id);
                FlightSearchResults singleDetail = db.FlightSearchResults.Where(p => p.id == thisid).FirstOrDefault();

                if (singleDetail != null && singleDetail.id > 0)
                {
                    FlightSearchResults newflightDel = new FlightSearchResults();

                    if (singleDetail.Avl_Seat != model.NoOfSeatApply)
                    {
                        newflightDel.Departure_Date = singleDetail.Departure_Date;
                        newflightDel.Arrival_Date = singleDetail.Arrival_Date;
                        newflightDel.Triptype = singleDetail.Triptype;
                        newflightDel.DepartureTerminal = singleDetail.DepartureTerminal;
                        newflightDel.ArrivalTerminal = singleDetail.ArrivalTerminal;
                        newflightDel.DepartureTime = singleDetail.DepartureTime;
                        newflightDel.ArrivalTime = singleDetail.ArrivalTime;
                        newflightDel.BagInfo = singleDetail.BagInfo;
                        newflightDel.FareRule = singleDetail.FareRule;
                        newflightDel.FareDet = singleDetail.FareDet;
                        newflightDel.FlightNo = singleDetail.FlightNo;
                        newflightDel.ClassType = singleDetail.ClassType;
                        newflightDel.OrgDestFrom = singleDetail.OrgDestFrom;
                        newflightDel.OrgDestTo = singleDetail.OrgDestTo;
                        newflightDel.AirLineName = singleDetail.AirLineName;
                        newflightDel.Rt_AirLineName = singleDetail.Rt_AirLineName;
                        newflightDel.Grand_Total = Convert.ToDouble(model.Amount);//set
                        newflightDel.DepAirportCode = singleDetail.DepAirportCode;
                        newflightDel.DepartureCityName = singleDetail.DepartureCityName;
                        newflightDel.DepartureAirportName = singleDetail.DepartureAirportName;
                        newflightDel.ArrivalCityName = singleDetail.ArrivalCityName;
                        newflightDel.ArrAirportCode = singleDetail.ArrAirportCode;
                        newflightDel.ArrivalAirportName = singleDetail.ArrivalAirportName;
                        newflightDel.MarketingCarrier = singleDetail.MarketingCarrier;
                        newflightDel.valid_Till = singleDetail.valid_Till;
                        newflightDel.RBD = singleDetail.RBD;
                        newflightDel.Total_Seats = model.NoOfSeatApply;//set
                        newflightDel.Used_Seat = "0";
                        newflightDel.Basicfare = Convert.ToDouble(model.Basefare);
                        newflightDel.YQ = Convert.ToDouble(model.YQ);
                        newflightDel.YR = Convert.ToDouble(model.YR);
                        newflightDel.WO = singleDetail.WO;
                        newflightDel.OT = Convert.ToDouble(model.OT);
                        newflightDel.GROSS_Total = 0;
                        newflightDel.Status = true;

                        newflightDel.InfFare = Convert.ToDouble(model.Infentfare);
                        newflightDel.infBfare = Convert.ToDouble(model.Infentfare);

                        newflightDel.MarkupWithGst = model.Markup2;
                        newflightDel.SupMarkup = model.Markup;
                        newflightDel.Markup_Type = singleDetail.Markup_Type;
                        newflightDel.Admin_Markup = 0;
                        newflightDel.TempStatus = singleDetail.TempStatus;
                        newflightDel.Markup = 0;
                        newflightDel.Avl_Seat = model.NoOfSeatApply;
                        newflightDel.fareBasis = singleDetail.fareBasis;
                        newflightDel.FixedDepStatus = singleDetail.FixedDepStatus;
                        newflightDel.CreatedByUserid = singleDetail.CreatedByUserid;
                        newflightDel.CreatedByName = singleDetail.CreatedByName;
                        newflightDel.IsPrimary = false;
                        db.FlightSearchResults.Add(newflightDel);
                        db.SaveChanges();

                        int createid = newflightDel.id;
                        if (createid > 0)
                        {
                            DataTable dt = new DataTable();
                            DataSet ds = new DataSet();
                            SqlDataAdapter adp = new SqlDataAdapter();
                            string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
                            SqlConnection con = new SqlConnection(constr);
                            SqlCommand cmd = new SqlCommand("FixDepatureCreate");
                            cmd.Connection = con;
                            con.Open();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@createid", createid);
                            adp.SelectCommand = cmd;
                            int temp = adp.Fill(ds);
                            cmd.Dispose();


                            string fieldswithvalue = "Total_Seats=" + (Convert.ToInt32(singleDetail.Total_Seats) - Convert.ToInt32(model.NoOfSeatApply)) + ",Avl_Seat = " + (Convert.ToInt32(singleDetail.Avl_Seat) - Convert.ToInt32(model.NoOfSeatApply));
                            string tablename = "FlightSearchResults";
                            string wherecondition = "Id = " + model.Id;
                            bool IsSuccess = UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition);
                            if (IsSuccess)
                            {
                                TempData["Msg"] = "Fare added successfully.";
                            }
                        }
                    }
                    else
                    {
                        string fieldswithvalue = "Grand_Total=" + model.Amount + ",Basicfare=" + model.Basefare + ",YQ=" + model.YQ + ",YR=" + model.YR + ",OT=" + model.OT + ",InfFare=" + model.Infentfare + ",infBfare=" + model.Infentfare + ",MarkupWithGst=" + model.Markup2 + ",SupMarkup=" + model.Markup + "";
                        string tablename = "FlightSearchResults";
                        string wherecondition = "Id = " + model.Id;
                        bool IsSuccess = UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition);
                        if (IsSuccess)
                        {
                            TempData["Msg"] = "Fare added successfully.";
                        }
                    }
                }
            }
            return View(model);
        }

        public JsonResult AddMoreSeatInExistingFare(string fareid, string newseatno)
        {
            string message = string.Empty;
            if (!string.IsNullOrEmpty(fareid) && !string.IsNullOrEmpty(newseatno))
            {
                int thisid = Convert.ToInt32(fareid);
                var singleDetail = db.FlightSearchResults.Where(p => p.id == thisid).FirstOrDefault();
                if (singleDetail != null && singleDetail.id > 0)
                {
                    int extSeatCount = Convert.ToInt32(newseatno);

                    //FlightSearchResults newflightDel = new FlightSearchResults();
                    //newflightDel.Total_Seats = (Convert.ToInt32(singleDetail.Total_Seats) + extSeatCount).ToString();//set
                    //newflightDel.Avl_Seat = (Convert.ToInt32(singleDetail.Avl_Seat) + extSeatCount).ToString();
                    //db.FlightSearchResults.Add(newflightDel);
                    //if (db.SaveChanges() > 0)
                    //{
                    //    message = "Seat added successfully in existing fare.";
                    //}

                    int totalSeat = Convert.ToInt32(singleDetail.Total_Seats) + extSeatCount;
                    int avlSeat = Convert.ToInt32(singleDetail.Avl_Seat) + extSeatCount;
                    string value = "Total_Seats = '" + totalSeat + "',Avl_Seat='" + avlSeat + "'";
                    string table = "FlightSearchResults";
                    string where = "id =" + fareid;
                    if (UpdateRecordIntoAnyTable(table, value, where))
                    {
                        message = "Seat added successfully in existing fare.";
                    }
                }
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAvlSeatDetailById(string fareid)
        {
            string message = string.Empty;
            if (!string.IsNullOrEmpty(fareid))
            {
                DataTable dtDetail = GetRecordFromTable("*", "FlightSearchResults", "id=" + fareid);
                if (dtDetail != null && dtDetail.Rows.Count > 0)
                {
                    message = dtDetail.Rows[0]["Avl_Seat"].ToString();
                }
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateFare(string id, string pnr)
        {
            UpdateFare model = new UpdateFare();
            if (!string.IsNullOrEmpty(id))
            {
                if (Session["userid"] != null)
                {
                    List<FlightSearchResults> stdList = new List<FlightSearchResults>();
                    if (!Convert.ToBoolean(Session["IsSupplierAdmin"].ToString()))
                    {
                        string userid = Session["userid"].ToString();
                        stdList = db.FlightSearchResults.Where(p => p.fareBasis == pnr && p.Grand_Total > 0 && p.Avl_Seat != "0" && p.CreatedByUserid == userid).OrderBy(p => p.id).ToList();
                    }
                    else
                    {
                        stdList = db.FlightSearchResults.Where(p => p.fareBasis == pnr && p.Grand_Total > 0 && p.Avl_Seat != "0").OrderBy(p => p.id).ToList();
                    }

                    if (stdList != null && stdList.Count > 0)
                    {
                        List<SelectListItem> listItems = new List<SelectListItem>();
                        foreach (var item in stdList)
                        {
                            listItems.Add(new SelectListItem
                            {
                                //Text = item.Grand_Total.ToString(),
                                Text = item.Grand_Total.ToString() + "," + item.Avl_Seat,
                                Value = item.id.ToString()
                            });
                        }

                        model.Id = id;
                        model.Pnr = pnr;
                        model.BasicFareList = listItems;
                    }
                }
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult UpdateFare(UpdateFare model, string Id)
        {
            try
            {
                if (model.SelectedOldFare != null)
                {
                    int thisid = Convert.ToInt32(model.SelectedOldFare);
                    var singleDetail = db.FlightSearchResults.Where(p => p.id == thisid).FirstOrDefault();
                    if (singleDetail != null && singleDetail.id > 0)
                    {
                        if (Convert.ToInt32(singleDetail.Avl_Seat) > 0)
                        {
                            FlightSearchResults newflightDel = new FlightSearchResults();

                            newflightDel.Departure_Date = singleDetail.Departure_Date;
                            newflightDel.Arrival_Date = singleDetail.Arrival_Date;
                            newflightDel.Triptype = singleDetail.Triptype;
                            newflightDel.DepartureTerminal = singleDetail.DepartureTerminal;
                            newflightDel.ArrivalTerminal = singleDetail.ArrivalTerminal;
                            newflightDel.DepartureTime = singleDetail.DepartureTime;
                            newflightDel.ArrivalTime = singleDetail.ArrivalTime;
                            newflightDel.BagInfo = singleDetail.BagInfo;
                            newflightDel.FareRule = singleDetail.FareRule;
                            newflightDel.FareDet = singleDetail.FareDet;
                            newflightDel.FlightNo = singleDetail.FlightNo;
                            newflightDel.ClassType = singleDetail.ClassType;
                            newflightDel.OrgDestFrom = singleDetail.OrgDestFrom;
                            newflightDel.OrgDestTo = singleDetail.OrgDestTo;
                            newflightDel.AirLineName = singleDetail.AirLineName;
                            newflightDel.Rt_AirLineName = singleDetail.Rt_AirLineName;
                            newflightDel.Grand_Total = Convert.ToDouble(model.Amount);//set
                            newflightDel.DepAirportCode = singleDetail.DepAirportCode;
                            newflightDel.DepartureCityName = singleDetail.DepartureCityName;
                            newflightDel.DepartureAirportName = singleDetail.DepartureAirportName;
                            newflightDel.ArrivalCityName = singleDetail.ArrivalCityName;
                            newflightDel.ArrAirportCode = singleDetail.ArrAirportCode;
                            newflightDel.ArrivalAirportName = singleDetail.ArrivalAirportName;
                            newflightDel.MarketingCarrier = singleDetail.MarketingCarrier;
                            newflightDel.valid_Till = singleDetail.valid_Till;
                            newflightDel.RBD = singleDetail.RBD;
                            newflightDel.Total_Seats = model.UpdateSeatCount;//set
                            newflightDel.Used_Seat = "0";
                            newflightDel.Basicfare = Convert.ToDouble(model.Basefare);
                            newflightDel.YQ = Convert.ToDouble(model.YQ);
                            newflightDel.YR = Convert.ToDouble(model.YR);
                            newflightDel.WO = singleDetail.WO;
                            newflightDel.OT = Convert.ToDouble(model.OT);
                            newflightDel.GROSS_Total = 0;
                            newflightDel.Status = true;

                            newflightDel.InfFare = Convert.ToDouble(model.InfantFare);
                            newflightDel.infBfare = Convert.ToDouble(model.InfantFare);

                            newflightDel.MarkupWithGst = "0"; //model.Markup2;
                            newflightDel.SupMarkup = "0";//model.Markup;
                            newflightDel.Markup_Type = singleDetail.Markup_Type;
                            newflightDel.Admin_Markup = 0;
                            newflightDel.TempStatus = singleDetail.TempStatus;
                            newflightDel.Markup = 0;
                            newflightDel.Avl_Seat = model.UpdateSeatCount;
                            newflightDel.fareBasis = singleDetail.fareBasis;
                            newflightDel.FixedDepStatus = singleDetail.FixedDepStatus;
                            newflightDel.CreatedByUserid = singleDetail.CreatedByUserid;
                            newflightDel.CreatedByName = singleDetail.CreatedByName;
                            newflightDel.IsPrimary = false;
                            db.FlightSearchResults.Add(newflightDel);
                            db.SaveChanges();

                            int createid = newflightDel.id;
                            if (createid > 0)
                            {
                                DataTable dt = new DataTable();
                                DataSet ds = new DataSet();
                                SqlDataAdapter adp = new SqlDataAdapter();
                                string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
                                SqlConnection con = new SqlConnection(constr);
                                SqlCommand cmd = new SqlCommand("FixDepatureCreate");
                                cmd.Connection = con;
                                con.Open();
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@createid", createid);
                                adp.SelectCommand = cmd;
                                int temp = adp.Fill(ds);
                                cmd.Dispose();


                                string fieldswithvalue = "Total_Seats=" + (Convert.ToInt32(singleDetail.Total_Seats) - Convert.ToInt32(model.UpdateSeatCount)) + ",Avl_Seat = " + (Convert.ToInt32(singleDetail.Avl_Seat) - Convert.ToInt32(model.UpdateSeatCount));
                                string tablename = "FlightSearchResults";
                                string wherecondition = "Id = " + model.SelectedOldFare;
                                bool IsSuccess = UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition);
                                if (IsSuccess)
                                {
                                    TempData["Msg"] = "Fare updated successfully.";
                                }
                            }
                        }
                        else
                        {
                            TempData["Msg"] = "Seat not available, please try again with another fare slab.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public JsonResult GetPnrDetail(string pnr, string fare)
        {
            if (!string.IsNullOrEmpty(fare))
            {
                var PnrDetail = db.FlightSearchResults.Where(p => p.fareBasis == pnr && p.id.ToString() == fare).OrderBy(p => p.id).FirstOrDefault();
                return Json(PnrDetail, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Markup1(string fddid, string markupOne, string markupTwo)
        {
            MarkupModel model = new MarkupModel();
            if (!string.IsNullOrEmpty(fddid))
            {
                model.FddId = Convert.ToInt32(fddid);
                model.SupMarkup = markupOne;
                model.MarkupWithGst = markupTwo;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Markup1(MarkupModel model)
        {

            string fieldswithvalue = "SupMarkup = '" + model.SupMarkup + "',MarkupWithGst = '" + model.MarkupWithGst + "'";
            string tablename = "Flightsearchresults";
            string wherecondition = "id = " + model.FddId;
            bool Isupdated = UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition);
            if (Isupdated)
            {
                TempData["Msg"] = "Markup Updated successfully";
            }
            return View(model);
        }
        public ActionResult MeangeFare(string id, string basefare, string YQ, string YR, string WO, string OT, string Markup, string GrandTotal)
        {
            FixdDepModel model = new FixdDepModel();
            model.Id = id;
            model.Basefare = basefare;
            model.YQ = YQ;
            model.YR = YR;
            model.WO = WO;
            model.OT = OT;
            model.Markup = Markup;
            model.Grand_total = GrandTotal;
            return View(model);
        }
        [HttpPost]
        public ActionResult MeangeFare(FixdDepModel model)
        {
            model.Markup = string.Empty;
            string fieldswithvalue = "Grand_Total=" + model.Grand_total + ",Basicfare=" + model.Basefare + ",YQ=" + model.YQ + ",YR=" + model.YR + ",WO=" + model.WO + ",OT=" + model.OT + "";
            string tablename = "FlightSearchResults";
            string wherecondition = "Id = " + model.Id;
            bool IsSuccess = UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition);
            if (IsSuccess)
            {
                TempData["Msg"] = "Fare Update";
            }
            return View(model);
        }

        public ActionResult TicketReport(FlightTicketFilter filter, string submitButton)
        {
            FlightTicketReportModel model = new FlightTicketReportModel();
            try
            {
                int totalcount = 0;
                filter.Status = StatusClass.Ticketed.ToString();
                filter.UserType = "Supp";
                filter.Suppliorid = Session["userid"].ToString();
                string stdate = "";
                string enddate = "";
                if (!string.IsNullOrEmpty(filter.FromDate))
                {
                    stdate = filter.FromDate.Split('/')[2] + "/" + filter.FromDate.Split('/')[1] + "/" + filter.FromDate.Split('/')[0];
                    filter.FromDate = stdate + " 12:00:00 AM";
                }
                if (!string.IsNullOrEmpty(filter.ToDate))
                {
                    enddate = filter.ToDate.Split('/')[2] + "/" + filter.ToDate.Split('/')[1] + "/" + filter.ToDate.Split('/')[0];
                    filter.ToDate = enddate + " 11:59:59 PM";
                }
                model.TicketReportlist = AccountHelper.GetFixDepTicketReports(filter, ref totalcount);
                if (submitButton == "EXPORT")
                {
                    DataSet tempDS = AccountHelper.GetTicektReport(filter);
                    if (tempDS != null)
                    {
                        DataTable dtLedDetail = tempDS.Tables[0];

                        DataTable dt = new DataTable();
                        dt.Clear();
                        dt.Columns.Add("ORDER ID");
                        dt.Columns.Add("PNR");
                        dt.Columns.Add("TICKET NO");
                        dt.Columns.Add("PAX NAME");
                        dt.Columns.Add("AIRLINE");
                        dt.Columns.Add("SECTOR");
                        dt.Columns.Add("BOOKING DATE");
                        dt.Columns.Add("JOURNEY DATE");
                        dt.Columns.Add("AMOUNT PER PAX");

                        if (dtLedDetail != null && dtLedDetail.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtLedDetail.Rows.Count; i++)
                            {
                                DataRow _ledger = dt.NewRow();
                                _ledger["ORDER ID"] = dtLedDetail.Rows[i]["OrderId"].ToString();
                                _ledger["PNR"] = dtLedDetail.Rows[i]["GdsPnr"].ToString();
                                _ledger["TICKET NO"] = dtLedDetail.Rows[i]["TicketNumber"].ToString();
                                _ledger["PAX NAME"] = dtLedDetail.Rows[i]["PName"].ToString();
                                _ledger["AIRLINE"] = dtLedDetail.Rows[i]["VC"].ToString();
                                _ledger["SECTOR"] = dtLedDetail.Rows[i]["sector"].ToString();
                                _ledger["BOOKING DATE"] = dtLedDetail.Rows[i]["CreateDate"].ToString();
                                _ledger["JOURNEY DATE"] = dtLedDetail.Rows[i]["JourneyDate"].ToString();
                                _ledger["AMOUNT PER PAX"] = dtLedDetail.Rows[i]["TotalAfterDis"].ToString();
                                dt.Rows.Add(_ledger);
                            }
                        }

                        DataSet dsNelwLedDel = new DataSet();
                        dsNelwLedDel.Tables.Add(dt);
                        Global.ExportData(dsNelwLedDel);
                    }
                }

                model.Totalcount = totalcount;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        public ActionResult LedgerReport(FlightLedgerFilter filter, string submitButton)
        {
            FlightLedgerReportModel model = new FlightLedgerReportModel();
            try
            {
                int totalcount = 0;
                //filter.Status = StatusClass.Ticketed.ToString();
                filter.UserType = "AGENT";
                filter.Loginid = Session["userid"].ToString();
                filter.SearchType = "Own";
                string stdate = "";
                string enddate = "";
                if (!string.IsNullOrEmpty(filter.FromDate))
                {
                    stdate = filter.FromDate.Split('/')[2] + "/" + filter.FromDate.Split('/')[1] + "/" + filter.FromDate.Split('/')[0];
                    filter.FromDate = stdate + " 12:00:00 AM";
                }
                if (!string.IsNullOrEmpty(filter.ToDate))
                {
                    enddate = filter.ToDate.Split('/')[2] + "/" + filter.ToDate.Split('/')[1] + "/" + filter.ToDate.Split('/')[0];
                    filter.ToDate = enddate + " 11:59:59 PM";
                }
                model.LedgerReportlist = AccountHelper.GetFixDepLedgerReports(filter, ref totalcount);
                if (submitButton == "EXPORT")
                {
                    DataSet tempDS = AccountHelper.GetLedgerReport(filter);
                    if (tempDS != null)
                    {
                        DataTable dtLedDetail = tempDS.Tables[0];

                        DataTable dt = new DataTable();
                        dt.Clear();
                        dt.Columns.Add("AGENCY ID");
                        dt.Columns.Add("INVOICE NO");
                        dt.Columns.Add("PNR");
                        dt.Columns.Add("DEBIT");
                        dt.Columns.Add("CREDIT");
                        dt.Columns.Add("BALANCE");
                        dt.Columns.Add("BOOKING TYPE");
                        dt.Columns.Add("CREATED DATE");
                        dt.Columns.Add("REMARK");

                        if (dtLedDetail != null && dtLedDetail.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtLedDetail.Rows.Count; i++)
                            {
                                DataRow _ledger = dt.NewRow();
                                _ledger["AGENCY ID"] = dtLedDetail.Rows[i]["AgencyID"].ToString();
                                _ledger["INVOICE NO"] = dtLedDetail.Rows[i]["InvoiceNo"].ToString();
                                _ledger["PNR"] = dtLedDetail.Rows[i]["PnrNo"].ToString();
                                _ledger["DEBIT"] = dtLedDetail.Rows[i]["Debit"].ToString();
                                _ledger["CREDIT"] = dtLedDetail.Rows[i]["Credit"].ToString();
                                _ledger["BALANCE"] = dtLedDetail.Rows[i]["Aval_Balance"].ToString();
                                _ledger["BOOKING TYPE"] = dtLedDetail.Rows[i]["BookingType"].ToString();
                                _ledger["CREATED DATE"] = dtLedDetail.Rows[i]["CreatedDate"].ToString();
                                _ledger["REMARK"] = dtLedDetail.Rows[i]["Remark"].ToString();
                                dt.Rows.Add(_ledger);
                            }
                        }

                        DataSet dsNelwLedDel = new DataSet();
                        dsNelwLedDel.Tables.Add(dt);
                        Global.ExportData(dsNelwLedDel);
                    }
                }
                model.Totalcount = totalcount;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        public ActionResult LedgerInvoice(string orderid)
        {
            DataTable dt_supplier = GetRecordFromTable("*", "Agent_register", "User_id='" + Session["userid"].ToString() + "'");
            DataTable dt_agentId = GetRecordFromTable("AgentId,TotalBookingCost,UpdateDate,Adult,Child,Infant", "Fltheader", "orderid='" + orderid + "'");
            DataTable dt_agency = GetRecordFromTable("*", "Agent_register", "User_id='" + dt_agentId.Rows[0]["AgentId"] + "'");

            DataTable dt_faredetails = GetRecordFromTable("*", "Fltfaredetails", "orderid='" + orderid + "'");
            DataTable dt_paxdetails = GetRecordFromTable("*", "Fltpaxdetails", "orderid='" + orderid + "'");

            ViewBag.SupplierName = dt_supplier.Rows[0]["Agency_Name"];
            ViewBag.SupplierAddress = dt_supplier.Rows[0]["Address"];
            ViewBag.SupplierCity = dt_supplier.Rows[0]["City"];
            ViewBag.SupplierState = dt_supplier.Rows[0]["State"];
            ViewBag.SupplierPincode = dt_supplier.Rows[0]["zipcode"];
            ViewBag.SupplierGSTNo = dt_supplier.Rows[0]["GSTNO"];
            ViewBag.SupplierContactNo = dt_supplier.Rows[0]["Mobile"];
            ViewBag.SupplierEmail = dt_supplier.Rows[0]["Email"];

            ViewBag.AgencyName = dt_agency.Rows[0]["Agency_Name"];
            ViewBag.AgencyAddress = dt_agency.Rows[0]["Address"];
            ViewBag.AgencyCity = dt_agency.Rows[0]["City"];
            ViewBag.AgencyState = dt_agency.Rows[0]["State"];
            ViewBag.AgencyPincode = dt_agency.Rows[0]["zipcode"];
            ViewBag.AgencyGSTNo = dt_agency.Rows[0]["GSTNO"];
            ViewBag.AgencyContactNo = dt_agency.Rows[0]["Mobile"];
            ViewBag.AgencyEmail = dt_agency.Rows[0]["Email"];

            ViewBag.GrossTotal = dt_agentId.Rows[0]["TotalBookingCost"];
            ViewBag.OrderId = orderid;
            ViewBag.CreatedDate = dt_agentId.Rows[0]["UpdateDate"];

            int adultCount = Convert.ToInt32(dt_agentId.Rows[0]["Adult"]);
            int ChildCount = Convert.ToInt32(dt_agentId.Rows[0]["Child"]);
            int InfantCount = Convert.ToInt32(dt_agentId.Rows[0]["Infant"]);

            ViewBag.AdultFare_Basic = 0.00;
            ViewBag.ChildFare_Basic = 0.00;
            ViewBag.InfantFare_Basic = 0.00;

            ViewBag.AdultFare_Tax = 0.00;
            ViewBag.ChildFare_Tax = 0.00;
            ViewBag.InfantFare_Tax = 0.00;

            ViewBag.AdultFare_Total = 0.00;
            ViewBag.ChildFare_Total = 0.00;
            ViewBag.InfantFare_Total = 0.00;

            ViewBag.AdultNames = new List<string>();
            ViewBag.ChildNames = new List<string>();
            ViewBag.InfantNames = new List<string>();

            DataTable tempTable = new DataTable();
            List<string> tempNames = new List<string>();

            if (adultCount > 0)
            {
                ViewBag.AdultFare_Basic = Convert.ToDecimal(dt_faredetails.Select("[PaxType] = 'ADT'").CopyToDataTable().Rows[0]["BaseFare"]).ToString("#.##");
                ViewBag.AdultFare_Tax = Convert.ToDecimal(dt_faredetails.Select("[PaxType] = 'ADT'").CopyToDataTable().Rows[0]["TotalTax"]).ToString("#.##");
                ViewBag.AdultFare_Total = Convert.ToDecimal(dt_faredetails.Select("[PaxType] = 'ADT'").CopyToDataTable().Rows[0]["TotalFare"]).ToString("#.##");

                tempTable = dt_paxdetails.Select("[PaxType] = 'ADT'").CopyToDataTable();
                for (int i = 0; i < tempTable.Rows.Count; i++)
                {
                    tempNames.Add(tempTable.Rows[i]["Title"].ToString() + " " + tempTable.Rows[i]["Fname"].ToString() + " " + tempTable.Rows[i]["Mname"].ToString() + " " + tempTable.Rows[i]["LName"].ToString());
                }
                ViewBag.AdultNames = tempNames;
            }
            if (ChildCount > 0)
            {
                ViewBag.ChildFare_Basic = Convert.ToDecimal(dt_faredetails.Select("[PaxType] = 'CHD'").CopyToDataTable().Rows[0]["BaseFare"]).ToString("#.##");
                ViewBag.ChildFare_Tax = Convert.ToDecimal(dt_faredetails.Select("[PaxType] = 'CHD'").CopyToDataTable().Rows[0]["TotalTax"]).ToString("#.##");
                ViewBag.ChildFare_Total = Convert.ToDecimal(dt_faredetails.Select("[PaxType] = 'CHD'").CopyToDataTable().Rows[0]["TotalFare"]).ToString("#.##");

                tempNames = new List<string>();
                tempTable = dt_paxdetails.Select("[PaxType] = 'CHD'").CopyToDataTable();
                for (int i = 0; i < tempTable.Rows.Count; i++)
                {
                    tempNames.Add(tempTable.Rows[i]["Title"].ToString() + " " + tempTable.Rows[i]["Fname"].ToString() + " " + tempTable.Rows[i]["Mname"].ToString() + " " + tempTable.Rows[i]["LName"].ToString());
                }
                ViewBag.ChildNames = tempNames;
            }
            if (InfantCount > 0)
            {
                ViewBag.InfantFare_Basic = Convert.ToDecimal(dt_faredetails.Select("[PaxType] = 'INF'").CopyToDataTable().Rows[0]["BaseFare"]).ToString("#.##");
                ViewBag.InfantFare_Tax = Convert.ToDecimal(dt_faredetails.Select("[PaxType] = 'INF'").CopyToDataTable().Rows[0]["TotalTax"]).ToString("#.##");
                ViewBag.InfantFare_Total = Convert.ToDecimal(dt_faredetails.Select("[PaxType] = 'INF'").CopyToDataTable().Rows[0]["TotalFare"]).ToString("#.##");

                tempNames = new List<string>();
                tempTable = dt_paxdetails.Select("[PaxType] = 'INF'").CopyToDataTable();
                for (int i = 0; i < tempTable.Rows.Count; i++)
                {
                    tempNames.Add(tempTable.Rows[i]["Title"].ToString() + " " + tempTable.Rows[i]["Fname"].ToString() + " " + tempTable.Rows[i]["Mname"].ToString() + " " + tempTable.Rows[i]["LName"].ToString());
                }
                ViewBag.InfantNames = tempNames;
            }

            return View();
        }

        public ActionResult LedgerReportallSupplior(FlightLedgerFilter filter, string submitButton)
        {
            FlightLedgerReportModel model = new FlightLedgerReportModel();
            try
            {
                int totalcount = 0;
                filter.UserType = "Supplior";
                filter.Loginid = Session["userid"].ToString();
                filter.SearchType = "Own";
                string stdate = "";
                string enddate = "";
                if (!string.IsNullOrEmpty(filter.FromDate))
                {
                    stdate = filter.FromDate.Split('/')[2] + "/" + filter.FromDate.Split('/')[1] + "/" + filter.FromDate.Split('/')[0];
                    filter.FromDate = stdate + " 12:00:00 AM";
                }
                if (!string.IsNullOrEmpty(filter.ToDate))
                {
                    enddate = filter.ToDate.Split('/')[2] + "/" + filter.ToDate.Split('/')[1] + "/" + filter.ToDate.Split('/')[0];
                    filter.ToDate = enddate + " 11:59:59 PM";
                }
                model.LedgerReportlist = AccountHelper.GetFlightAllSuppliorLedgerReports(filter, ref totalcount);
                if (submitButton == "EXPORT")
                {
                    DataSet tempDS = AccountHelper.GetLedgerReportAllSuppior(filter);
                    if (tempDS != null)
                    {
                        DataTable dtLedDetail = tempDS.Tables[0];

                        DataTable dt = new DataTable();
                        dt.Clear();
                        dt.Columns.Add("SUPPLIER ID");
                        dt.Columns.Add("SUPPLIER NAME");
                        dt.Columns.Add("INVOICE NO");
                        dt.Columns.Add("PNR");
                        dt.Columns.Add("DEBIT");
                        dt.Columns.Add("CREDIT");
                        dt.Columns.Add("BALANCE");
                        dt.Columns.Add("BOOKING TYPE");
                        dt.Columns.Add("CREATED DATE");
                        dt.Columns.Add("REMARK");

                        if (dtLedDetail != null && dtLedDetail.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtLedDetail.Rows.Count; i++)
                            {
                                DataRow _ledger = dt.NewRow();
                                _ledger["SUPPLIER ID"] = dtLedDetail.Rows[i]["AgencyID"].ToString();
                                _ledger["SUPPLIER NAME"] = dtLedDetail.Rows[i]["AgencyName"].ToString();
                                _ledger["INVOICE NO"] = dtLedDetail.Rows[i]["InvoiceNo"].ToString();
                                _ledger["PNR"] = dtLedDetail.Rows[i]["PnrNo"].ToString();
                                _ledger["DEBIT"] = dtLedDetail.Rows[i]["Debit"].ToString();
                                _ledger["CREDIT"] = dtLedDetail.Rows[i]["Credit"].ToString();
                                _ledger["BALANCE"] = dtLedDetail.Rows[i]["Aval_Balance"].ToString();
                                _ledger["BOOKING TYPE"] = dtLedDetail.Rows[i]["BookingType"].ToString();
                                _ledger["CREATED DATE"] = dtLedDetail.Rows[i]["CreatedDate"].ToString();
                                _ledger["REMARK"] = dtLedDetail.Rows[i]["Remark"].ToString();
                                dt.Rows.Add(_ledger);
                            }
                        }

                        DataSet dsNelwLedDel = new DataSet();
                        dsNelwLedDel.Tables.Add(dt);
                        Global.ExportData(dsNelwLedDel);
                    }
                }
                model.Totalcount = totalcount;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        public ActionResult UpdatPnr(string OrderId, string totalcost)
        {
            FixdDepHoldBookingModel model = new FixdDepHoldBookingModel();
            model.OrderId = OrderId;
            model.totalcost = totalcost;
            return View(model);
        }
        [HttpPost]
        public ActionResult UpdatPnr(FixdDepHoldBookingModel model)
        {
            DataTable MailDt = new DataTable();
            model.AgentId = Session["userid"].ToString();
            MailDt = Global.GetMailingDetails("AIR_PNRSUMMARY", "").Tables[0];
            if (!string.IsNullOrEmpty(model.AgentId))
            {
                string fileds = "*";
                string tablename = "Agent_register";
                string whereCondition = "User_id='" + model.AgentId + "'";
                DataTable map = GetRecordFromTable(fileds, tablename, whereCondition);
                if (map.Rows.Count > 0)
                {
                    string agentid = !string.IsNullOrEmpty(map.Rows[0]["AgencyId"].ToString()) ? map.Rows[0]["AgencyId"].ToString() : string.Empty;
                    string AgencyName = !string.IsNullOrEmpty(map.Rows[0]["Agency_Name"].ToString()) ? map.Rows[0]["Agency_Name"].ToString() : string.Empty;
                    string AgencyEmail = !string.IsNullOrEmpty(map.Rows[0]["Email"].ToString()) ? map.Rows[0]["Email"].ToString() : string.Empty;

                    List<string> flag1 = Global.InsertCreditDebitIntoLedgerTable(Convert.ToDouble(model.totalcost), model.AgentId, AgencyName, model.OrderId, model.GdsPnr, model.GdsPnr, "", "", model.AgentId, "", "::1", 0, Convert.ToDouble(model.totalcost), "Credit", "Amount Credit to Your Account from fixed departure", 0, "CR", "", 0, "Confirm", "", "Amount Credit to Your Account from fixed departure", "", "", "Flt_Ticket_Amount", 0, "");
                    string fieldswithvalue = "GdsPnr = '" + model.GdsPnr.ToUpper() + "',Status='Ticketed',Remark='" + model.Remark + "',UpdateDate=GETDATE()";
                    string tablenames = "FltHeader";
                    string whereconditions = "OrderId ='" + model.OrderId + "'";
                    bool IsSuccess = UpdateRecordIntoAnyTable(tablenames, fieldswithvalue, whereconditions);
                    string value = "PnrNo = '" + model.GdsPnr.ToUpper() + "',UpdatedDate=GETDATE()";
                    string table = "ledgerDetails";
                    string where = "InvoiceNo ='" + model.OrderId + "'";
                    bool IsSuccess1 = UpdateRecordIntoAnyTable(table, value, where);
                    string strFileNmPdf = "";
                    string mailbody = Global.TicketCopyExportPDF(model.OrderId, "");
                    string TicketFormate;
                    bool writePDF = false;
                    try
                    {
                        TicketFormate = mailbody.Trim().ToString();
                        strFileNmPdf = ConfigurationManager.AppSettings["HTMLtoPDF"].ToString().Trim() + model.OrderId + "-" + DateTime.Now.ToString().Replace(":", "").Replace("/", "-").Replace(" ", "-").Trim() + ".pdf";
                        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A4);
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, new FileStream(strFileNmPdf, FileMode.Create, FileAccess.ReadWrite, FileShare.None));
                        pdfDoc.Open();
                        StringReader sr = new StringReader(TicketFormate);
                        iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                        pdfDoc.Close();
                        writer.Dispose();
                        sr.Dispose();
                        pdfDoc.Dispose();
                        writePDF = true;
                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                    }
                    int i = Global.SendMail(AgencyEmail, MailDt.Rows[0]["MAILFROM"].ToString(), "", "", MailDt.Rows[0]["SMTPCLIENT"].ToString(), MailDt.Rows[0]["UserId"].ToString(), MailDt.Rows[0]["Pass"].ToString(), mailbody, MailDt.Rows[0]["SUBJECT"].ToString(), strFileNmPdf);

                    if (i == 1)
                    {
                        TempData["Msg"] = "Pnr Update";
                    }
                    else
                    {
                        TempData["Msg"] = "something issue in sending mail please contact it administrator.. ";
                    }
                }
            }
            return View(model);
        }

        public ActionResult RejectPnr(string OrderId)
        {
            FixdDepHoldBookingModel model = new FixdDepHoldBookingModel();
            model.OrderId = OrderId;
            return View(model);
        }
        [HttpPost]
        public ActionResult RejectPnr(FixdDepHoldBookingModel model)
        {
            if (!string.IsNullOrEmpty(model.OrderId))
            {
                string fieldswithvalue = "Status='Rejected',Remark='" + model.Remark + "',UpdateDate=GETDATE()";
                string tablenames = "FltHeader";
                string whereconditions = "OrderId ='" + model.OrderId + "'";
                bool IsSuccess = UpdateRecordIntoAnyTable(tablenames, fieldswithvalue, whereconditions);
                if (IsSuccess)
                {
                    TempData["Msg"] = "Rejected  successfully";
                }
                else
                {
                    TempData["Msg"] = "something issue in sending mail please contact it administrator.. ";
                }
            }
            return View(model);
        }
        public ActionResult HoldPnrRequest(FlightTicketFilter filter)
        {
            FlightTicketReportModel model = new FlightTicketReportModel();
            try
            {
                int totalcount = 0;
                filter.Suppliorid = Session["userid"].ToString();
                model.TicketReportlist = AccountHelper.GetFlightHoldTicketReports(filter, ref totalcount);
                model.Totalcount = totalcount;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        public ActionResult Supplerlogin(string userdetail)
        {
            Session["IsSupplierAdmin"] = false;
            if (!string.IsNullOrEmpty(userdetail))
            {
                string[] userdel = userdetail.Split('|');
                string userid = userdel[0];
                string password = userdel[1];

                DataTable dtAgentDel = AccountHelper.GetAgentRegisterDetails(userid, password);
                if (dtAgentDel != null && dtAgentDel.Rows.Count > 0)
                {
                    string ColName = string.Empty;
                    if (dtAgentDel.Columns.Contains("UID"))
                    {
                        ColName = "UID";
                    }
                    if (dtAgentDel.Columns.Contains("PWD"))
                    {
                        ColName = "PWD";
                    }
                    if (dtAgentDel.Columns.Contains("Password"))
                    {
                        ColName = string.Empty;
                    }


                    if (string.IsNullOrEmpty(ColName))
                    {
                        bool IsSupplier = Convert.ToBoolean(dtAgentDel.Rows[0]["IsSupplier"].ToString());
                        bool IsSupplierAdmin = Convert.ToBoolean(dtAgentDel.Rows[0]["IsSupplierAdmin"].ToString());
                        if (IsSupplier)
                        {
                            string R1 = Encrypt(userid + "-" + password + "-" + DateTime.Now.ToString("yyMMddHHmmssff"));
                            if (AccountHelper.InsertQuery(R1))
                            {
                                if (IsSupplierAdmin)
                                {
                                    Session["isExecutive"] = 1;
                                    Session["IsSupplierAdmin"] = true;
                                    return RedirectToAction("SupplierPnr", new { R1 = R1 });
                                }
                                {
                                    Session["isExecutive"] = 1;
                                    return RedirectToAction("Index", new { R1 = R1 });

                                }
                            }
                        }
                        else
                        {
                            TempData["Message"] = "You are not authoried to access this service.";
                        }
                    }
                    else
                    {
                        TempData["Message"] = dtAgentDel.Rows[0][ColName].ToString();
                    }
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult Supplerlogin(Account model)
        {
            Session["IsSupplierAdmin"] = false;
            string message = string.Empty;
            DataTable dtAgentDel = AccountHelper.GetAgentRegisterDetails(model.UserId, model.Password);

            if (dtAgentDel != null && dtAgentDel.Rows.Count > 0)
            {
                string ColName = string.Empty;
                if (dtAgentDel.Columns.Contains("UID"))
                {
                    ColName = "UID";
                }
                if (dtAgentDel.Columns.Contains("PWD"))
                {
                    ColName = "PWD";
                }
                if (dtAgentDel.Columns.Contains("Password"))
                {
                    ColName = string.Empty;
                }


                if (string.IsNullOrEmpty(ColName))
                {
                    bool IsSupplier = Convert.ToBoolean(dtAgentDel.Rows[0]["IsSupplier"].ToString());
                    bool IsSupplierAdmin = Convert.ToBoolean(dtAgentDel.Rows[0]["IsSupplierAdmin"].ToString());

                    //Session["IsSupplierAdmin"] = dtAgentDel.Rows[0]["IsSupplierAdmin"].ToString();
                    if (IsSupplier)
                    {
                        string R1 = Encrypt(model.UserId + "-" + model.Password + "-" + DateTime.Now.ToString("yyMMddHHmmssff"));
                        if (AccountHelper.InsertQuery(R1))
                        {
                            if (IsSupplierAdmin)
                            {
                                Session["isExecutive"] = 1;
                                Session["IsSupplierAdmin"] = true;
                                return RedirectToAction("SupplierPnr", new { R1 = R1 });
                            }
                            {
                                Session["isExecutive"] = 1;
                                return RedirectToAction("Index", new { R1 = R1 });

                            }
                        }
                    }
                    else
                    {
                        TempData["Message"] = "You are not authoried to access this service.";
                    }
                }
                else
                {
                    TempData["Message"] = dtAgentDel.Rows[0][ColName].ToString();
                }
            }


            return View();
        }

        public ActionResult Flight_Search(string Sector)
        {

            if (Convert.ToString(Session["userid"]) == "")
            {
                Response.Redirect("/");
            }
            ViewBag.secor = Sector;
            return View();
        }


        public ActionResult Sector()
        {
            if (Convert.ToString(Session["userid"]) == "")
            {
                Response.Redirect("/");
            }
            return View();
        }

        public ActionResult Logout()
        {
            string redirectUrl = string.Empty;

            if (Session["isExecutive"] != null)
            {
                redirectUrl = "/";
            }
            else
            {
                redirectUrl = "/";
            }

            Session.Abandon();
            Session.Clear();
            if (Convert.ToString(Session["userid"]) == "")
            {
                //Response.Redirect(redirectUrl);                 
                //Response.Redirect("http://uat.tripmaza.com/");
                Response.Redirect("https://tripforo.com/");
            }
            return View();
        }

        public ActionResult Backhome()
        {
            string R2 = "";
            if (Convert.ToString(Session["userid"]) != "")
            {

                R2 = Encrypt(Session["userid"] + "-" + Session["Code"]);

            }
            //http://support.ghaitravels.com/Login.aspx?R2=" + R2 + "
            return Json("/");

        }
        // GET: FlightSearchResults
        public ActionResult Index(string from, string to, string travelDatestart, string travelDateEnd, string Pnr, string AirLine, string flightno)
        {
            string matchkey = "";
            List<FlightSearchResults> stdList;
            int totalskip;
            string UserID = "";
            string pwd = "";
            string R1 = "";
            string rr = "";

            bool no = false;


            if (Request.QueryString["R1"] != null)
            {
                if (AccountHelper.InsertQuery(Request.QueryString["R1"].ToString()))
                {
                    matchkey = QueryKeyI(Request.QueryString["R1"]);
                }
            }

            //  Response.Write("<script>alert('" + rr + "');</script>");



            if (matchkey == "NO")
            {
                no = true;

                if (Convert.ToString(Session["query"]) != null)
                {
                    try
                    {
                        if (Request.QueryString["R1"] != null && no == false)
                        {

                            if (Convert.ToString(Session["R1"]) == Convert.ToString(Request.QueryString["R1"]))
                            {
                                R1 = Session["R1"].ToString();
                                UserID = R1.Split('-')[0];
                                pwd = R1.Split('-')[1];
                            }
                            else
                            {
                                R1 = Decrypt(Request.QueryString["R1"].ToString());
                                Session["R1"] = R1;
                                UserID = R1.Split('-')[0];
                                pwd = R1.Split('-')[1];
                            }

                        }
                        else if (Request.QueryString["R1"] == null && no == false)
                        {
                            if (Convert.ToString(Session["R1"]) == "")
                            {
                                //Redirect Action
                            }
                            else
                            {
                                R1 = Session["R1"].ToString();
                                UserID = R1.Split('-')[0];
                                pwd = R1.Split('-')[1];
                            }
                        }
                        else
                        {
                            // UserID = Decrypt(Request.QueryString["UserID"].ToString());
                            //pwd = Decrypt(Request.QueryString["Code"].ToString());
                        }




                        int temp = validUser(UserID, pwd);

                        if (Session["isExecutive"] != null)
                        {
                            temp = 1;
                        }

                        if (temp > 0)
                        {
                            if (Convert.ToString(Session["R1"]) == "")
                            {
                                Session["userid"] = UserID;
                                Session["Code"] = pwd;
                                Session["R1"] = UserID + "-" + pwd;
                                Session["query"] = UserID;
                                return RedirectToAction("Sector");
                            }
                            else
                            {
                                Session["userid"] = UserID;
                                Session["Code"] = pwd;

                            }




                        }
                        else
                        {

                            Response.Redirect("/");

                        }

                    }
                    catch
                    {
                        Response.Redirect("/");

                    }

                }
                else
                {
                    Response.Redirect("/");
                }
            }
            else if (matchkey == "YES" || Convert.ToString(Session["query"]) != null)
            {


                try
                {
                    if (Request.QueryString["R1"] != null && no == false)
                    {

                        if (Convert.ToString(Session["R1"]) == Convert.ToString(Request.QueryString["R1"]))
                        {
                            R1 = Session["R1"].ToString();
                            UserID = R1.Split('-')[0];
                            pwd = R1.Split('-')[1];
                        }
                        else
                        {
                            R1 = Decrypt(Request.QueryString["R1"].ToString());
                            Session["R1"] = R1;
                            UserID = R1.Split('-')[0];
                            pwd = R1.Split('-')[1];
                        }

                    }
                    else if (Request.QueryString["R1"] == null && no == false)
                    {
                        if (Convert.ToString(Session["R1"]) == "")
                        {
                            //Redirect Action
                        }
                        else
                        {
                            R1 = Session["R1"].ToString();
                            UserID = R1.Split('-')[0];
                            pwd = R1.Split('-')[1];
                        }
                    }
                    else
                    {
                        // UserID = Decrypt(Request.QueryString["UserID"].ToString());
                        //pwd = Decrypt(Request.QueryString["Code"].ToString());
                    }




                    int temp = validUser(UserID, pwd);
                    if (Session["isExecutive"] != null)
                    {
                        temp = 1;
                    }
                    if (temp > 0)
                    {
                        if (Convert.ToString(Session["R1"]) == "")
                        {
                            Session["userid"] = UserID;
                            Session["Code"] = pwd;
                            Session["R1"] = UserID + "-" + pwd;
                            Session["query"] = UserID;
                            return RedirectToAction("Sector");
                        }
                        else
                        {
                            Session["userid"] = UserID;
                            Session["Code"] = pwd;

                        }




                    }
                    else
                    {

                        Response.Redirect("/");

                    }

                }
                catch
                {
                    Response.Redirect("/");

                }
            }

            GetIndex(from, to, travelDatestart, travelDateEnd, Pnr, AirLine, flightno, out stdList);
            return View(stdList);
        }

        protected int validUser(string userID, string PWD)
        {

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            SqlDataAdapter adp = new SqlDataAdapter();


            string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
            SqlConnection con = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand("UserLogin_PP");
            cmd.Connection = con;
            con.Open();

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Usename", userID);
            cmd.Parameters.AddWithValue("@Password", PWD);


            adp.SelectCommand = cmd;
            int temp = adp.Fill(ds);
            cmd.Dispose();
            return temp;

        }

        protected string QueryKeyI(string QueryKey)
        {
            string ReturnQUERY = "";
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            SqlDataAdapter adp = new SqlDataAdapter();


            string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
            SqlConnection con = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand("SP_QueryKey");
            cmd.Connection = con;
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@QueryKey", QueryKey.Replace(" ", "+"));
            cmd.Parameters.AddWithValue("@Action", "S");
            ReturnQUERY = Convert.ToString(cmd.ExecuteScalar());
            cmd.Dispose();
            return ReturnQUERY;

        }

        private string Decrypt(string cipherText)
        {

            cipherText = cipherText.Replace(" ", "+");
            string EncryptionKey = "MAKV2SPBNI99520";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {

                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
        private string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99520";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        //public PartialViewResult NextResult(int? page, string from, string to)
        //{
        //    IQueryable<FlightSearchResults> stdList;
        //    int totalskip;
        //    GetIndex(page, from, to, out stdList, out totalskip);
        //    return PartialView(stdList.Skip(totalskip));
        //    //return PartialView(stdList);
        //}
        private void GetIndex(string from, string to, string travelDatestart, string travelDateEnd, string Pnr, string AirLine, string flightno, out List<FlightSearchResults> stdList)
        {
            stdList = new List<FlightSearchResults>();
            string UserID = Session["userid"].ToString();

            if (!string.IsNullOrEmpty(from) || !string.IsNullOrEmpty(to) || !string.IsNullOrEmpty(travelDatestart) || !string.IsNullOrEmpty(travelDateEnd) || !string.IsNullOrEmpty(Pnr) || !string.IsNullOrEmpty(AirLine) || !string.IsNullOrEmpty(flightno))
            {
                stdList = db.FlightSearchResults.Where(p => p.CreatedByUserid == UserID).OrderBy(b => b.Departure_Date).ToList();
            }

            if (!string.IsNullOrEmpty(from))
            {
                stdList = stdList.Where(p => p.OrgDestFrom == from && p.CreatedByUserid == UserID && p.TempStatus == true).OrderBy(b => b.Departure_Date).ToList();
            }

            if (!string.IsNullOrEmpty(to))
            {
                stdList = stdList.Where(p => p.OrgDestTo == to && p.CreatedByUserid == UserID && p.TempStatus == true).OrderBy(b => b.Departure_Date).ToList();
            }

            if (!string.IsNullOrEmpty(travelDatestart))
            {
                string stdate = travelDatestart.Split('/')[2] + "-" + travelDatestart.Split('/')[1] + "-" + travelDatestart.Split('/')[0];
                stdList = stdList.Where(b => Convert.ToDateTime(b.Departure_Date.Replace("-", "/").Split('/')[2] + "-" + b.Departure_Date.Replace("-", "/").Split('/')[1] + "-" + b.Departure_Date.Replace("-", "/").Split('/')[0]) >= Convert.ToDateTime(stdate) && b.CreatedByUserid == UserID && b.TempStatus == true).OrderBy(b => b.Departure_Date).ToList();
            }

            if (!string.IsNullOrEmpty(travelDateEnd))
            {
                string todate = travelDateEnd.Split('/')[2] + "-" + travelDateEnd.Split('/')[1] + "-" + travelDateEnd.Split('/')[0];
                stdList = stdList.Where(b => Convert.ToDateTime(b.Departure_Date.Replace("-", "/").Split('/')[2] + "-" + b.Departure_Date.Replace("-", "/").Split('/')[1] + "-" + b.Departure_Date.Replace("-", "/").Split('/')[0]) <= Convert.ToDateTime(todate) && b.CreatedByUserid == UserID && b.TempStatus == true).OrderBy(b => b.Departure_Date).ToList();
            }

            if (!string.IsNullOrEmpty(Pnr))
            {
                stdList = stdList.Where(p => p.fareBasis == Pnr && p.CreatedByUserid == UserID && p.TempStatus == true).ToList();
            }
            if (!string.IsNullOrEmpty(AirLine))
            {
                stdList = stdList.Where(p => p.AirLineName == AirLine && p.CreatedByUserid == UserID && p.TempStatus == true).OrderBy(b => b.Departure_Date).ToList();
            }
            if (!string.IsNullOrEmpty(flightno))
            {
                stdList = stdList.Where(p => p.FlightNo == flightno && p.CreatedByUserid == UserID && p.TempStatus == true).OrderBy(b => b.Departure_Date).ToList();
            }

            ViewBag.from = from;
            ViewBag.To = to;
            ViewBag.travelstartDate = travelDatestart;
            ViewBag.travelDaendte = travelDateEnd;
            ViewBag.Airline = AirLine;
            ViewBag.flightno = flightno;
            ViewBag.Pnr = Pnr;
            ViewBag.totalrecord = stdList.Count();
        }

        private void GetSupplierIndex(string from, string to, string travelDatestart, string travelDateEnd, string supplierid, string Pnr, string AirLine, string flightno, out List<FlightSearchResults> stdList)
        {
            stdList = new List<FlightSearchResults>();
            string UserID = Session["userid"].ToString();

            if (!string.IsNullOrEmpty(from) || !string.IsNullOrEmpty(to) || !string.IsNullOrEmpty(travelDatestart) || !string.IsNullOrEmpty(travelDateEnd) || !string.IsNullOrEmpty(supplierid) || !string.IsNullOrEmpty(Pnr) || !string.IsNullOrEmpty(AirLine) || !string.IsNullOrEmpty(flightno))
            {
                stdList = db.FlightSearchResults.Where(b => b.Departure_Date != null).OrderBy(b => b.Departure_Date).ToList();
            }

            if (!string.IsNullOrEmpty(from))
            {
                stdList = stdList.Where(p => p.OrgDestFrom == from && p.TempStatus == true).OrderBy(b => b.Departure_Date).ToList();
            }

            if (!string.IsNullOrEmpty(to))
            {
                stdList = stdList.Where(p => p.OrgDestTo == to && p.TempStatus == true).OrderBy(b => b.Departure_Date).ToList();
            }

            if (!string.IsNullOrEmpty(travelDatestart))
            {
                string stdate = travelDatestart.Split('/')[2] + "-" + travelDatestart.Split('/')[1] + "-" + travelDatestart.Split('/')[0];
                stdList = stdList.Where(b => Convert.ToDateTime(b.Departure_Date.Replace("-", "/").Split('/')[2] + "-" + b.Departure_Date.Replace("-", "/").Split('/')[1] + "-" + b.Departure_Date.Replace("-", "/").Split('/')[0]) >= Convert.ToDateTime(stdate) && b.TempStatus == true).OrderBy(b => b.Departure_Date).ToList();
            }

            if (!string.IsNullOrEmpty(travelDateEnd))
            {
                string todate = travelDateEnd.Split('/')[2] + "-" + travelDateEnd.Split('/')[1] + "-" + travelDateEnd.Split('/')[0];
                stdList = stdList.Where(b => Convert.ToDateTime(b.Departure_Date.Replace("-", "/").Split('/')[2] + "-" + b.Departure_Date.Replace("-", "/").Split('/')[1] + "-" + b.Departure_Date.Replace("-", "/").Split('/')[0]) <= Convert.ToDateTime(todate) && b.TempStatus == true).OrderBy(b => b.Departure_Date).ToList();
            }
            if (!string.IsNullOrEmpty(Pnr))
            {
                stdList = stdList.Where(p => p.fareBasis == Pnr.ToUpper() && p.TempStatus == true).OrderBy(b => b.Departure_Date).ToList();
            }
            if (!string.IsNullOrEmpty(supplierid))
            {
                stdList = stdList.Where(p => p.CreatedByUserid == supplierid && p.TempStatus == true).OrderBy(b => b.Departure_Date).ToList();
            }
            if (!string.IsNullOrEmpty(AirLine))
            {
                stdList = stdList.Where(p => p.AirLineName == AirLine && p.TempStatus == true).OrderBy(b => b.Departure_Date).ToList();
            }
            if (!string.IsNullOrEmpty(flightno))
            {
                stdList = stdList.Where(p => p.FlightNo == flightno && p.TempStatus == true).OrderBy(b => b.Departure_Date).ToList();
            }

            ViewBag.from = from;
            ViewBag.To = to;
            ViewBag.travelstartDate = travelDatestart;
            ViewBag.travelDaendte = travelDateEnd;
            ViewBag.Airline = AirLine;
            ViewBag.flightno = flightno;
            ViewBag.Pnr = Pnr;
            ViewBag.supplier = supplierid;
            ViewBag.totalrecord = stdList.Count();
        }

        // GET: FlightSearchResults/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FlightSearchResults flightSearchResults = db.FlightSearchResults.Find(id);
            if (flightSearchResults == null)
            {
                return HttpNotFound();
            }
            return View(flightSearchResults);
        }


        public ActionResult Create()
        {
            if (Convert.ToString(Session["userid"]) == "")
            {
                Response.Redirect("/");
            }

            return View();
        }
        [HttpPost]
        public JsonResult GetCityData(string Prefix)
        {

            var GetCityData = (from c in db.CityList
                               where c.CityName.StartsWith(Prefix) || c.AirportName.StartsWith(Prefix) || c.AirportCode.StartsWith(Prefix)
                               select new
                               {
                                   c.AirportName,
                                   c.AirportCode,
                                   c.CityName

                               }).ToList();



            if (GetCityData != null)
            {
                //  var BindInf = GetCityData.
            }


            return Json(GetCityData, JsonRequestBehavior.AllowGet);
        }
        // POST: FlightSearchResults/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FlightSearchResults flightSearchResults)
        {
            string createdId = string.Empty;
            try
            {
                // if (ModelState.IsValid)999
                // {
                //yyyy-MM-dd
                //DataTable agentinfo = new DataTable();
                //agentinfo= Global.SelectAgencyDetail(Session["userid"].ToString());

                string stdate = flightSearchResults.Departure_Date.Split('/')[2] + "/" + flightSearchResults.Departure_Date.Split('/')[1] + "/" + flightSearchResults.Departure_Date.Split('/')[0];

                string enddate = flightSearchResults.Arrival_Date.Split('/')[2] + "/" + flightSearchResults.Arrival_Date.Split('/')[1] + "/" + flightSearchResults.Arrival_Date.Split('/')[0];
                DateTime fromDate = Convert.ToDateTime(stdate);//21/01/2021
                DateTime toDate = Convert.ToDateTime(enddate);

                int totaldays = (toDate - fromDate).Days;
                DateTime tempDateTime = fromDate;

                string[] splited = flightSearchResults.OrgDestFrom.Split(',');
                string[] splited2 = flightSearchResults.OrgDestTo.Split(',');
                string[] splited3 = flightSearchResults.AirLineName.Split(',');
                string[] splited4 = new string[3];
                if (flightSearchResults.Triptype == "R")
                {
                    splited4 = flightSearchResults.Rt_AirLineName.Split(',');
                }

                for (int i = 1; i <= (totaldays + 1); i++)
                {
                    if (i != 1)
                    {
                        tempDateTime = tempDateTime.AddDays(1);
                        flightSearchResults.Departure_Date = tempDateTime.ToString("dd/MM/yyyy").Replace("-", "/");
                        flightSearchResults.Arrival_Date = tempDateTime.ToString("dd/MM/yyyy").Replace("-", "/");
                    }
                    else
                    {
                        flightSearchResults.Departure_Date = tempDateTime.ToString("dd/MM/yyyy").Replace("-", "/");
                        flightSearchResults.Arrival_Date = tempDateTime.ToString("dd/MM/yyyy").Replace("-", "/");
                    }

                    decimal gtotal = Math.Floor(Convert.ToDecimal(flightSearchResults.Grand_Total));
                    flightSearchResults.OrgDestFrom = splited[0] + "," + splited[2];
                    flightSearchResults.OrgDestTo = splited2[0] + "," + splited2[2];
                    flightSearchResults.AirLineName = splited3[0];

                    if (flightSearchResults.Triptype == "R")
                    {
                        flightSearchResults.Rt_AirLineName = splited4[0];
                    }

                    flightSearchResults.Grand_Total = Convert.ToDouble(gtotal);
                    //flightSearchResults.DepAirportCode = flightSearchResults.OrgDestFrom;
                    flightSearchResults.DepAirportCode = splited[2];

                    //flightSearchResults.DepartureCityName = flightSearchResults.OrgDestFrom;
                    flightSearchResults.DepartureCityName = splited[0];

                    //flightSearchResults.DepartureAirportName = flightSearchResults.OrgDestFrom;
                    flightSearchResults.DepartureAirportName = splited[1];

                    //flightSearchResults.ArrivalCityName = flightSearchResults.OrgDestTo;
                    flightSearchResults.ArrivalCityName = splited2[0];

                    //flightSearchResults.ArrAirportCode = flightSearchResults.OrgDestTo;
                    flightSearchResults.ArrAirportCode = splited2[2];

                    //flightSearchResults.ArrivalAirportName = flightSearchResults.OrgDestTo;
                    flightSearchResults.ArrivalAirportName = splited2[1];

                    flightSearchResults.MarketingCarrier = splited3[1];
                    flightSearchResults.valid_Till = toDate;
                    flightSearchResults.RBD = "0";
                    flightSearchResults.Total_Seats = flightSearchResults.Avl_Seat;
                    flightSearchResults.Used_Seat = "0";
                    flightSearchResults.Basicfare = 0;
                    flightSearchResults.YQ = 0;
                    flightSearchResults.YR = 0;
                    flightSearchResults.WO = 0;
                    flightSearchResults.OT = 0;
                    flightSearchResults.GROSS_Total = 0;
                    flightSearchResults.MarkupWithGst = "0";
                    flightSearchResults.SupMarkup = "0";
                    flightSearchResults.Markup_Type = "Flat";
                    flightSearchResults.Admin_Markup = 0;
                    flightSearchResults.TempStatus = true;
                    flightSearchResults.Markup = 0;
                    flightSearchResults.Avl_Seat = "0";
                    flightSearchResults.fareBasis = "";
                    flightSearchResults.Grand_Total = 0;
                    flightSearchResults.FixedDepStatus = true;
                    flightSearchResults.CreatedByUserid = Session["userid"].ToString();
                    flightSearchResults.IsPrimary = true;

                    DataTable dt_supplier = GetRecordFromTable("*", "Agent_register", "User_id='" + Session["userid"].ToString() + "'");
                    if (dt_supplier != null && dt_supplier.Rows.Count > 0)
                    {
                        flightSearchResults.CreatedByName = dt_supplier.Rows[0]["Agency_Name"].ToString();
                    }
                    db.FlightSearchResults.Add(flightSearchResults);
                    db.SaveChanges();

                    int createid = flightSearchResults.id;

                    createdId = createdId + (!string.IsNullOrEmpty(createdId) ? ("," + createid) : createid.ToString());

                    DataTable dt = new DataTable();
                    DataSet ds = new DataSet();
                    SqlDataAdapter adp = new SqlDataAdapter();
                    string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
                    SqlConnection con = new SqlConnection(constr);
                    SqlCommand cmd = new SqlCommand("FixDepatureCreate");
                    cmd.Connection = con;
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@createid", createid);
                    adp.SelectCommand = cmd;
                    int temp = adp.Fill(ds);
                    cmd.Dispose();
                }
                TempData["Msg"] = 1;
                // }
            }
            catch (Exception ex)
            {
                TempData["errorMsg"] = "Create_" + ex.Message + "_" + flightSearchResults.Departure_Date + "_" + flightSearchResults.Arrival_Date;

            }
            return RedirectToAction("PNRUpdate", new { ids = createdId });
        }

        public ActionResult PNRUpdate(string ids)
        {
            List<FlightSearchResults> stdList = new List<FlightSearchResults>();
            try
            {
                if (!string.IsNullOrEmpty(ids))
                {
                    string[] createdIds = ids.Split(',');
                    List<int> intIds = new List<int>();
                    foreach (var item in createdIds)
                    {
                        intIds.Add(Convert.ToInt32(item));
                    }

                    stdList = db.FlightSearchResults.Where(p => intIds.Contains(p.id)).ToList(); //.Where(p => ids.Contains(p.id));
                }
            }
            catch (Exception ex)
            {
                TempData["errorMsg"] = "PNRUpdate_" + ex.Message;
            }

            return View(stdList);
        }
        // GET: FlightSearchResults/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Convert.ToString(Session["userid"]) == "")
            {
                Response.Redirect("/");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FlightSearchResults flightSearchResults = db.FlightSearchResults.Find(id);
            if (flightSearchResults == null)
            {
                return HttpNotFound();
            }
            return View(flightSearchResults);
        }

        // POST: FlightSearchResults/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FlightSearchResults flightSearchResults)
        {
            try
            {
                if (!string.IsNullOrEmpty(flightSearchResults.fareBasis))
                {

                    string stdate = flightSearchResults.Departure_Date.Split('/')[2] + "/" + flightSearchResults.Departure_Date.Split('/')[1] + "/" + flightSearchResults.Departure_Date.Split('/')[0];

                    string enddate = flightSearchResults.Arrival_Date.Split('/')[2] + "/" + flightSearchResults.Arrival_Date.Split('/')[1] + "/" + flightSearchResults.Arrival_Date.Split('/')[0];
                    DateTime fromDate = Convert.ToDateTime(stdate);//21/01/2021
                    DateTime toDate = Convert.ToDateTime(enddate);
                    DateTime tempDateTimefrom = fromDate;
                    DateTime tempDateTimeTo = toDate;
                    flightSearchResults.Departure_Date = tempDateTimefrom.ToString("dd/MM/yyyy").Replace("-", "/");
                    flightSearchResults.Arrival_Date = tempDateTimeTo.ToString("dd/MM/yyyy").Replace("-", "/");
                    flightSearchResults.FixedDepStatus = true;
                    flightSearchResults.Connect = false;
                    flightSearchResults.TempStatus = true;

                    List<FlightSearchResults> stdList = new List<FlightSearchResults>();
                    string UserID = Session["userid"].ToString();
                    stdList = db.FlightSearchResults.Where(p => p.fareBasis == flightSearchResults.fareBasis).ToList();
                    if (stdList != null && stdList.Count > 0)
                    {
                        foreach (var item in stdList)
                        {
                            using (var dbContext = new myAmdDB())
                            {
                                if (flightSearchResults.id != item.id)
                                {
                                    flightSearchResults.id = item.id;
                                    flightSearchResults.IsPrimary = item.IsPrimary;
                                    flightSearchResults.Triptype = item.Triptype;
                                    flightSearchResults.BagInfo = item.BagInfo;
                                    flightSearchResults.FareRule = item.FareRule;
                                    flightSearchResults.FareDet = item.FareDet;
                                    flightSearchResults.ClassType = item.ClassType;
                                    flightSearchResults.OrgDestFrom = item.OrgDestFrom;
                                    flightSearchResults.OrgDestTo = item.OrgDestTo;
                                    flightSearchResults.AirLineName = item.AirLineName;
                                    flightSearchResults.Rt_AirLineName = item.Rt_AirLineName;
                                    flightSearchResults.Grand_Total = item.Grand_Total;
                                    flightSearchResults.DepAirportCode = item.DepAirportCode;
                                    flightSearchResults.DepartureCityName = item.DepartureCityName;
                                    flightSearchResults.DepartureAirportName = item.DepartureAirportName;
                                    flightSearchResults.ArrivalCityName = item.ArrivalCityName;
                                    flightSearchResults.ArrAirportCode = item.ArrAirportCode;
                                    flightSearchResults.ArrivalAirportName = item.ArrivalAirportName;
                                    flightSearchResults.MarketingCarrier = item.MarketingCarrier;
                                    flightSearchResults.valid_Till = item.valid_Till;
                                    flightSearchResults.RBD = item.RBD;
                                    flightSearchResults.Total_Seats = item.Total_Seats;//added seat
                                    flightSearchResults.Used_Seat = item.Used_Seat;
                                    flightSearchResults.Basicfare = item.Basicfare;
                                    flightSearchResults.YQ = item.YQ;
                                    flightSearchResults.YR = item.YR;
                                    flightSearchResults.WO = item.WO;
                                    flightSearchResults.OT = item.OT;
                                    flightSearchResults.GROSS_Total = item.GROSS_Total;
                                    flightSearchResults.Status = item.Status;
                                    flightSearchResults.InfFare = item.InfFare;
                                    flightSearchResults.infBfare = item.infBfare;
                                    flightSearchResults.MarkupWithGst = item.MarkupWithGst;
                                    flightSearchResults.SupMarkup = item.SupMarkup;
                                    flightSearchResults.Markup_Type = item.Markup_Type;
                                    flightSearchResults.Admin_Markup = item.Admin_Markup;
                                    flightSearchResults.TempStatus = item.TempStatus;
                                    flightSearchResults.Markup = item.Markup;
                                    flightSearchResults.Avl_Seat = item.Avl_Seat;
                                    flightSearchResults.fareBasis = item.fareBasis;
                                    flightSearchResults.FixedDepStatus = item.FixedDepStatus;
                                    flightSearchResults.CreatedByUserid = item.CreatedByUserid;
                                    flightSearchResults.CreatedByName = item.CreatedByName;
                                    flightSearchResults.RefNo = item.RefNo;
                                    flightSearchResults.SequenceNumber = item.SequenceNumber;
                                }
                                dbContext.Entry(flightSearchResults).State = EntityState.Modified;
                                dbContext.SaveChanges();
                            }
                        }
                    }
                }

                TempData["Msg"] = 2;
            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Duplicate_Id"))
                    TempData["Msg"] = 0;
            }
            if (Convert.ToBoolean(Session["IsSupplierAdmin"].ToString()))
            {
                return RedirectToAction("SupplierPnr");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        // GET: FlightSearchResults/Delete/5
        public ActionResult Delete(int? id)
        {
            FlightSearchResults flightSearchResults = null;

            if (Convert.ToString(Session["userid"]) == "")
            {
                Response.Redirect("/");
            }
            else
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                flightSearchResults = db.FlightSearchResults.Find(id);
                if (flightSearchResults == null)
                {
                    return HttpNotFound();
                }

            }



            return View(flightSearchResults);
        }

        // POST: FlightSearchResults/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            string fieldswithvalue = "TempStatus =0,FixedDepStatus=0";
            string tablename = "FlightSearchResults";
            string wherecondition = "Id = " + id;
            bool IsSuccess = UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition);
            //FlightSearchResults flightSearchResults = db.FlightSearchResults.Find(id);
            //db.FlightSearchResults.Remove(flightSearchResults);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }


        public ActionResult Connect(int? id)
        {
            if (Convert.ToString(Session["userid"]) == "")
            {
                Response.Redirect("/");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FlightSearchResults flightSearchResults = db.FlightSearchResults.Find(id);
            flightSearchResults.OrgDestFrom = string.Empty;
            flightSearchResults.OrgDestTo = string.Empty;
            if (flightSearchResults == null)
            {
                return HttpNotFound();
            }
            return View(flightSearchResults);
        }


        // POST: FlightSearchResults/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Connect(FlightSearchResults flightSearchResults)
        {
            try
            {
                //  if (ModelState.IsValid)
                // {
                //decimal gtotal = Math.Floor(Convert.ToDecimal(flightSearchResults.Grand_Total));
                //string[] splitedData = flightSearchResults.OrgDestFrom.Split(',');
                //flightSearchResults.OrgDestFrom = splitedData[0] + "," + splitedData[1];

                //string[] splitedData2 = flightSearchResults.OrgDestTo.Split(',');
                //flightSearchResults.OrgDestTo = splitedData2[0] + "," + splitedData2[1];

                //string[] splitedData3 = flightSearchResults.AirLineName.Split(',');

                //flightSearchResults.AirLineName = splitedData3[0];


                //flightSearchResults.Grand_Total = Convert.ToDouble(gtotal);


                //flightSearchResults.DepAirportCode = flightSearchResults.OrgDestFrom;
                //flightSearchResults.DepAirportCode = splitedData[1];

                //flightSearchResults.DepartureCityName = flightSearchResults.OrgDestFrom;
                //flightSearchResults.DepartureCityName = splitedData[0];


                //flightSearchResults.DepartureAirportName = flightSearchResults.DepartureAirportName;


                //flightSearchResults.ArrivalCityName = flightSearchResults.OrgDestTo;
                //flightSearchResults.ArrivalCityName = splitedData2[0];

                //flightSearchResults.ArrAirportCode = flightSearchResults.OrgDestTo;
                //flightSearchResults.ArrAirportCode = splitedData2[1];

                //flightSearchResults.ArrivalAirportName = flightSearchResults.ArrivalAirportName;

                //flightSearchResults.MarketingCarrier = flightSearchResults.MarketingCarrier;

                //// db.Entry(flightSearchResults).State = EntityState.Modified;
                //db.FlightSearchResults.Add(flightSearchResults);
                //db.SaveChanges();

                string createdId = string.Empty;
                int existid = flightSearchResults.id;


                decimal gtotal = Math.Floor(Convert.ToDecimal(flightSearchResults.Grand_Total));
                string[] splitedData = flightSearchResults.OrgDestFrom.Split(',');
                flightSearchResults.OrgDestFrom = splitedData[0] + "," + splitedData[2];

                string[] splitedData2 = flightSearchResults.OrgDestTo.Split(',');
                flightSearchResults.OrgDestTo = splitedData2[0] + "," + splitedData2[2];

                string[] splitedData3 = flightSearchResults.AirLineName.Split(',');

                flightSearchResults.AirLineName = splitedData3[0];
                flightSearchResults.Grand_Total = Convert.ToDouble(gtotal);
                flightSearchResults.DepAirportCode = splitedData[2];
                flightSearchResults.DepartureCityName = splitedData[0];
                flightSearchResults.DepartureAirportName = splitedData[1];
                flightSearchResults.ArrivalCityName = splitedData2[0];
                flightSearchResults.ArrAirportCode = splitedData2[2];
                flightSearchResults.ArrivalAirportName = splitedData2[1];
                flightSearchResults.CreatedByUserid = Session["userid"].ToString();
                flightSearchResults.FixedDepStatus = true;
                db.FlightSearchResults.Add(flightSearchResults);
                db.SaveChanges();

                int newid = flightSearchResults.id;

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();
                SqlDataAdapter adp = new SqlDataAdapter();
                string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
                SqlConnection con = new SqlConnection(constr);
                SqlCommand cmd = new SqlCommand("FixDepatureConnect");
                cmd.Connection = con;
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@existid", existid);
                cmd.Parameters.AddWithValue("@newid", newid);
                adp.SelectCommand = cmd;
                int temp = adp.Fill(ds);
                cmd.Dispose();

                // TempData["Msg"] = 2;
                //     }

            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Duplicate_Id"))
                    TempData["Msg"] = 0;
            }
            return RedirectToAction("Index");

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public class PNR
        {
            public string ArrivalDate { get; set; }
            public string PNRValue { get; set; }
            public string PNRId { get; set; }
            public string SeatAvail { get; set; }
            public string Amount { get; set; }
            public string Basefare { get; set; }
            public string YQ { get; set; }
            public string YR { get; set; }
            public string OT { get; set; }
            public string WO { get; set; }
            public string SupMarkup { get; set; }
            public string MarkupWithGst { get; set; }
            public string InfantFare { get; set; }

        }

        public class AgencyBalance
        {
            public string AgencyName { get; set; }
            public decimal Balance { get; set; }
        }

        public static bool INSERTMISCSRV_SUPPLIORWithGST(MarkupModel model)
        {

            try
            {
                bool withGST = true;
                string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
                SqlConnection con = new SqlConnection(constr);
                SqlCommand Command = new SqlCommand("sp_INSERTMISCSRV_SUPPLIORWithGST", con);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("@Airline", model.Airline);
                Command.Parameters.AddWithValue("@Trip", "D");
                Command.Parameters.AddWithValue("@AgentId", "ALL");
                Command.Parameters.AddWithValue("@Amount", Convert.ToDecimal(model.Amount));
                Command.Parameters.AddWithValue("@GroupType", "ALL");
                Command.Parameters.AddWithValue("@Org", model.Org);
                Command.Parameters.AddWithValue("@Dest", model.Dest);
                Command.Parameters.AddWithValue("@FareType", "FDD");
                Command.Parameters.AddWithValue("@MarkupType", model.MarkupType);
                Command.Parameters.AddWithValue("@CommisionOnBasic", Convert.ToDecimal(model.CommisionOnBasic));
                Command.Parameters.AddWithValue("@CommisionOnBasicYq", Convert.ToDecimal(model.CommisionOnBasicYq));
                Command.Parameters.AddWithValue("@CommissionOnYq", Convert.ToDecimal(model.CommissionOnYq));
                Command.Parameters.AddWithValue("@CreatedBy", model.CreatedBy);
                Command.Parameters.AddWithValue("@FddId", model.FddId);
                Command.Parameters.AddWithValue("@WithGST", withGST);
                Command.Parameters.AddWithValue("@FlightNo", model.FlightNo);
                con.Open();
                int isSuccess = Command.ExecuteNonQuery();
                con.Close();
                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool INSERTMISCSRV_SUPPLIOR(MarkupModel model)
        {

            try
            {
                string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
                SqlConnection con = new SqlConnection(constr);
                SqlCommand Command = new SqlCommand("sp_INSERTMISCSRV_SUPPLIOR", con);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("@Airline", model.Airline);
                Command.Parameters.AddWithValue("@Trip", "D");
                Command.Parameters.AddWithValue("@AgentId", "ALL");
                Command.Parameters.AddWithValue("@Amount", Convert.ToDecimal(model.Amount));
                Command.Parameters.AddWithValue("@GroupType", "ALL");
                Command.Parameters.AddWithValue("@Org", model.Org);
                Command.Parameters.AddWithValue("@Dest", model.Dest);
                Command.Parameters.AddWithValue("@FareType", "FDD");
                Command.Parameters.AddWithValue("@MarkupType", model.MarkupType);
                Command.Parameters.AddWithValue("@CommisionOnBasic", Convert.ToDecimal(model.CommisionOnBasic));
                Command.Parameters.AddWithValue("@CommisionOnBasicYq", Convert.ToDecimal(model.CommisionOnBasicYq));
                Command.Parameters.AddWithValue("@CommissionOnYq", Convert.ToDecimal(model.CommissionOnYq));
                Command.Parameters.AddWithValue("@CreatedBy", model.CreatedBy);
                Command.Parameters.AddWithValue("@FddId", model.FddId);
                Command.Parameters.AddWithValue("@FlightNo", model.FlightNo);
                con.Open();
                int isSuccess = Command.ExecuteNonQuery();
                con.Close();
                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool UpdateRecordIntoAnyTable(string tablename, string fieldswithvalue, string wherecondition)
        {
            try
            {
                string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
                SqlConnection con = new SqlConnection(constr);
                SqlCommand cmd = new SqlCommand("ups_UpdateRecordIntoAnyTable", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("TableName", tablename);
                cmd.Parameters.AddWithValue("FieldsWithValue", fieldswithvalue);
                cmd.Parameters.AddWithValue("WhereCondition", wherecondition);
                con.Open();
                int isSuccess = cmd.ExecuteNonQuery();
                con.Close();
                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static DataTable GetRecordFromTable(string fileds, string tablename, string whereCondition)
        {
            DataSet ObjDataSet = new DataSet();
            DataTable ObjDataTable = new DataTable();
            try
            {
                string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
                SqlConnection con = new SqlConnection(constr);
                SqlCommand cmd = new SqlCommand("usp_GetRecordFromTable", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("fileds", fileds);
                cmd.Parameters.AddWithValue("tablename", tablename);
                cmd.Parameters.AddWithValue("where", whereCondition);
                SqlDataAdapter Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = cmd;
                Adapter.Fill(ObjDataSet, "CommonTable");
                ObjDataTable = ObjDataSet.Tables["CommonTable"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return ObjDataTable;
        }
        public JsonResult PNRInserter(List<PNR> PNRList)
        {
            string result = "";

            for (int i = 0; i < PNRList.Count(); i++)
            {
                string fieldswithvalue = "fareBasis = '" + PNRList[i].PNRValue + "',InfFare = " + PNRList[i].InfantFare + ",infBfare = " + PNRList[i].InfantFare + ",Arrival_Date = '" + PNRList[i].ArrivalDate + "' ,Avl_Seat = '" + PNRList[i].SeatAvail + "',Total_Seats = '" + PNRList[i].SeatAvail + "',Grand_Total=" + PNRList[i].Amount + ",Basicfare=" + PNRList[i].Basefare + ",YQ=" + PNRList[i].YQ + ",YR=" + PNRList[i].YR + ",OT=" + PNRList[i].OT + ",SupMarkup='" + PNRList[i].SupMarkup + "',MarkupWithGst='" + PNRList[i].MarkupWithGst + "'";
                string tablename = "FlightSearchResults";
                string wherecondition = "Id = " + PNRList[i].PNRId;

                bool IsSuccess = UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition);
                if (IsSuccess) { result = "Success"; }
            }
            return Json(result);
        }

        //public JsonResult PNRInserter(List<PNR> InitialList, List<PNR> BucketList)
        //{
        //    string result = "";

        //    try
        //    {
        //        for (int i = 0; i < InitialList.Count(); i++)
        //        {
        //            string fieldswithvalue = "fareBasis = '" + InitialList[i].PNRValue + "',InfFare = " + InitialList[i].InfantFare + ",infBfare = " + InitialList[i].InfantFare + ",Arrival_Date = '" + InitialList[i].ArrivalDate + "' ,Avl_Seat = '" + InitialList[i].SeatAvail + "',Total_Seats = '" + InitialList[i].SeatAvail + "',Grand_Total=" + InitialList[i].Amount + ",Basicfare=" + InitialList[i].Basefare + ",YQ=" + InitialList[i].YQ + ",YR=" + InitialList[i].YR + ",OT=" + InitialList[i].OT + ",SupMarkup='" + InitialList[i].SupMarkup + "',MarkupWithGst='" + InitialList[i].MarkupWithGst + "'";
        //            string tablename = "FlightSearchResults";
        //            string wherecondition = "Id = " + InitialList[i].PNRId;

        //            bool IsSuccess = UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition);
        //            if (IsSuccess) { result = "Success"; }
        //        }

        //        if (BucketList != null && BucketList.Count > 0)
        //        {
        //            for (int b = 0; b < BucketList.Count(); b++)
        //            {
        //                result = "";
        //                int currid = Convert.ToInt32(BucketList[b].PNRId);
        //                FlightSearchResults singleDetail = db.FlightSearchResults.Where(p => p.id == currid).FirstOrDefault();

        //                if (singleDetail != null && singleDetail.id > 0)
        //                {
        //                    FlightSearchResults newflightDel = new FlightSearchResults();

        //                    newflightDel.Departure_Date = singleDetail.Departure_Date;
        //                    newflightDel.Arrival_Date = singleDetail.Arrival_Date;
        //                    newflightDel.Triptype = singleDetail.Triptype;
        //                    newflightDel.DepartureTerminal = singleDetail.DepartureTerminal;
        //                    newflightDel.ArrivalTerminal = singleDetail.ArrivalTerminal;
        //                    newflightDel.DepartureTime = singleDetail.DepartureTime;
        //                    newflightDel.ArrivalTime = singleDetail.ArrivalTime;
        //                    newflightDel.BagInfo = singleDetail.BagInfo;
        //                    newflightDel.FareRule = singleDetail.FareRule;
        //                    newflightDel.FareDet = singleDetail.FareDet;
        //                    newflightDel.FlightNo = singleDetail.FlightNo;
        //                    newflightDel.ClassType = singleDetail.ClassType;
        //                    newflightDel.OrgDestFrom = singleDetail.OrgDestFrom;
        //                    newflightDel.OrgDestTo = singleDetail.OrgDestTo;
        //                    newflightDel.AirLineName = singleDetail.AirLineName;
        //                    newflightDel.Rt_AirLineName = singleDetail.Rt_AirLineName;
        //                    newflightDel.Grand_Total = Convert.ToDouble(BucketList[b].Amount);//set
        //                    newflightDel.DepAirportCode = singleDetail.DepAirportCode;
        //                    newflightDel.DepartureCityName = singleDetail.DepartureCityName;
        //                    newflightDel.DepartureAirportName = singleDetail.DepartureAirportName;
        //                    newflightDel.ArrivalCityName = singleDetail.ArrivalCityName;
        //                    newflightDel.ArrAirportCode = singleDetail.ArrAirportCode;
        //                    newflightDel.ArrivalAirportName = singleDetail.ArrivalAirportName;
        //                    newflightDel.MarketingCarrier = singleDetail.MarketingCarrier;
        //                    newflightDel.valid_Till = singleDetail.valid_Till;
        //                    newflightDel.RBD = singleDetail.RBD;
        //                    newflightDel.Total_Seats = BucketList[b].SeatAvail;//set
        //                    newflightDel.Used_Seat = "0";
        //                    newflightDel.Basicfare = Convert.ToDouble(BucketList[b].Basefare);
        //                    newflightDel.YQ = Convert.ToDouble(BucketList[b].YQ);
        //                    newflightDel.YR = Convert.ToDouble(BucketList[b].YR);
        //                    newflightDel.WO = singleDetail.WO;
        //                    newflightDel.OT = Convert.ToDouble(BucketList[b].OT);
        //                    newflightDel.GROSS_Total = 0;
        //                    newflightDel.Status = true;

        //                    newflightDel.InfFare = Convert.ToDouble(BucketList[b].InfantFare);
        //                    newflightDel.infBfare = Convert.ToDouble(BucketList[b].InfantFare);

        //                    newflightDel.MarkupWithGst = BucketList[b].MarkupWithGst;
        //                    newflightDel.SupMarkup = BucketList[b].SupMarkup;
        //                    newflightDel.Markup_Type = singleDetail.Markup_Type;
        //                    newflightDel.Admin_Markup = 0;
        //                    newflightDel.TempStatus = singleDetail.TempStatus;
        //                    newflightDel.Markup = 0;
        //                    newflightDel.Avl_Seat = BucketList[b].SeatAvail;
        //                    newflightDel.fareBasis = BucketList[b].PNRValue;
        //                    newflightDel.FixedDepStatus = singleDetail.FixedDepStatus;
        //                    newflightDel.CreatedByUserid = singleDetail.CreatedByUserid;
        //                    db.FlightSearchResults.Add(newflightDel);
        //                    db.SaveChanges();

        //                    int createid = newflightDel.id;
        //                    if (createid > 0)
        //                    {
        //                        DataTable dt = new DataTable();
        //                        DataSet ds = new DataSet();
        //                        SqlDataAdapter adp = new SqlDataAdapter();
        //                        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        //                        SqlConnection con = new SqlConnection(constr);
        //                        SqlCommand cmd = new SqlCommand("FixDepatureCreate");
        //                        cmd.Connection = con;
        //                        con.Open();
        //                        cmd.CommandType = CommandType.StoredProcedure;
        //                        cmd.Parameters.AddWithValue("@createid", createid);
        //                        adp.SelectCommand = cmd;
        //                        int temp = adp.Fill(ds);
        //                        cmd.Dispose();
        //                        result = "Success";
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //    }

        //    return Json(result);
        //}

        public JsonResult Getsuppliorbalance()
        {
            List<object> GetgencyData = new List<object>();
            string userid = Session["userid"].ToString();
            if (!string.IsNullOrEmpty(userid))
            {
                DataTable dtbalance = AccountHelper.GetSupliorbalance(userid);
                if (dtbalance.Rows.Count > 0)
                {
                    string AgencyName = !string.IsNullOrEmpty(dtbalance.Rows[0]["Agency_Name"].ToString()) ? dtbalance.Rows[0]["Agency_Name"].ToString() : string.Empty;
                    decimal Balance = !string.IsNullOrEmpty(dtbalance.Rows[0]["Crd_Limit"].ToString()) ? Convert.ToDecimal(dtbalance.Rows[0]["Crd_Limit"].ToString()) : 0;

                    GetgencyData.Add(AgencyName);
                    GetgencyData.Add(Balance);
                }
            }
            return Json(GetgencyData);
        }

        public JsonResult StatusUpdator(string farebasis, bool status)
        {
            int FixedDepStatus = 0;

            if (status)
            {
                FixedDepStatus = 1;
            }
            else
            {
                FixedDepStatus = 0;
            }
            List<object> result = new List<object>();
            string fieldswithvalue = "FixedDepStatus = " + FixedDepStatus + "";
            string tablename = "FlightSearchResults";
            string wherecondition = "fareBasis = '" + farebasis + "'";
            bool IsSuccess = UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition);
            if (IsSuccess)
            {
                result.Add("Success");
            }
            return Json(result);
        }

    }
}
